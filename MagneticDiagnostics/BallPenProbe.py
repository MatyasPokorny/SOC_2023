# FILE FOR PLOTTING BALL-PEN PROBE DATA AND DETERMINING PLASMA POSITION
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "Times New Roman"
"""
Usage:
1. input shot session to shotNumbers varibale
2. input t_init, t_step, t_num variables to determine character of plasma potential radial profile
3. change BPP_radialPositions varibale accoring to used shot session (-105 = most distant R [mm] coordinate of BPP from chamber center; 5 = step in R [mm] between shots)

! While data loading, the script looks into your files and tries to find the requested plasma potential radial profile. If it doesn't find it, it loads it and saves it (paths have to be changed).

! Script is functional for the old shot session analysed during SOČ 2022/23. For newer sessions data loading has to be changed.
"""

#--------------
# INPUT PARAMS:

shotNumbers = [26355,26356,26357,26358,26359,26360,26361,26362,26366,26365]
numberOfShots = len(shotNumbers)

t_init = 9.0  # first time value
t_step = 1.0  # time value, over which we're averaging
t_num  = 5    # number of time segments

BPP_radialPositions = [-105+i*5 for i in range(numberOfShots)] # HAS TO BE MANUALLY CHANGED WHEN CHANGING SESSION [mm]






#--------
# SCRIPT:

def loadRadialProfileBPP(t_min, t_max):    
    try:
        plasmaPotential_radialProfile = pd.read_csv(f"/home/maty/Plocha/TokGol_2022_23/python/plasmaPosition_figures/BPP_DATA/BPP_radial_profile_session_{shotNumbers[0]}_{shotNumbers[-1]}_time_{t_min}_{t_max}.csv")
        plasmaPotential_radialProfile = list(plasmaPotential_radialProfile["0"])
        return plasmaPotential_radialProfile
    except:
        plasmaPotential_radialProfile = []
        for i in range(numberOfShots):
            BPP_signal = pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{shotNumbers[i]}/bpp", sep="\t", names=["time", "U_fl"])
            LP_signal  = pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{shotNumbers[i]}/lp", sep="\t", names=["time", "U_fl"])
            BPP_signal["time"] = BPP_signal["time"] * 1000
            BPP_signal["U_fl"] = BPP_signal["U_fl"] / 10
            LP_signal["U_fl"] = LP_signal["U_fl"]   / 10
            plasmaPotential = pd.DataFrame(BPP_signal["U_fl"] - ((BPP_signal["U_fl"] - LP_signal["U_fl"]) / 2.0))
            plasmaPotential = plasmaPotential.set_index(BPP_signal["time"], drop=True)
            plasmaPotential_radialProfile.append( sum(plasmaPotential["U_fl"][t_min:t_max]) )
            print("loading done")
        plasmaPotential_radialProfile = pd.Series(plasmaPotential_radialProfile)
        plasmaPotential_radialProfile.to_csv(f"/home/maty/Plocha/TokGol_2022_23/python/plasmaPosition_figures/BPP_DATA/BPP_radial_profile_session_{shotNumbers[0]}_{shotNumbers[-1]}_time_{t_min}_{t_max}.csv")
        return plasmaPotential_radialProfile

def plotRadialProfile(BPP_radialProfilesArray, t_minArray, t_maxArray):    
    fig, ax = plt.subplots(1,1, figsize=(9*1.1,8*1.1))
    for i in range(len(BPP_radialProfilesArray)):
        ax.scatter(BPP_radialPositions, BPP_radialProfilesArray[i], marker="x", label = f"${t_minArray[i]}-{t_maxArray[i]}$ ms")
        ax.plot(BPP_radialPositions, BPP_radialProfilesArray[i], linestyle="--", linewidth=0.75)
    ax.set_title(fr"\textbf{{Potenciál plazmatu ($\mathbf{{\#26355-\#26366}}$)}}", size=20)
    # ax.invert_xaxis()
    # ax.set_xlim(ax.get_xlim()[::-1])
    ax.set_xlabel(r"$Z \; \mathrm{[mm]}$", size=20)
    ax.set_ylabel(r"$\Phi_p \; \mathrm{[V]}$", size=20)
    ax.grid(alpha=0.25)
    ax.tick_params(axis = "both", labelsize = 15)
    ax.legend(prop={'size': 15}, frameon=False)
    # plt.savefig("/home/maty/Plocha/TokGol_2022_23/python/plasmaPosition_figures/BPP_DATA/BPP_graf.pdf")
    plt.show()

plasmaPotential_radialProfilesArray = [] 
t_minArray = []
t_maxArray = []
for i in range(t_num):
    plasmaPotential_radialProfilesArray.append(loadRadialProfileBPP(t_init + i, t_init + t_step + i))
    t_minArray.append(t_init + i)
    t_maxArray.append(t_init + t_step + i)

plotRadialProfile(plasmaPotential_radialProfilesArray, t_minArray, t_maxArray)
