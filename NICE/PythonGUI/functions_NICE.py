import subprocess
import os
import xml.dom.minidom as md
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from GlobalData import GD


def set_plasma_boudnary(center):
    """
    Function writes desired boundary into desired_bnd.txt file.

    center ... array of the form [R_c, Z_c]
    radius ... integer
    """
    plasma_radius = GD.GOLEM_limiter_radius - np.sqrt( (GD.R_chamber_center - center[0])**2 + (GD.Z_chamber_center - center[1])**2 )
    theta = np.linspace(0, 2*np.pi, 100)
    theta = np.delete(theta, -1)

    plasma_boundary_R = plasma_radius * np.cos(theta) + center[0]
    plasma_boundary_Z = plasma_radius * np.sin(theta) + center[1]
    plasma_boundary_R = np.round_(plasma_boundary_R, 15)
    plasma_boundary_Z = np.round_(plasma_boundary_Z, 15)

    os.chdir(GD.path_to_GUI)
    os.chdir("../input")
    desired_bnd_file = open("desired_bnd.txt", "w")
    desired_bnd_file.write(str(len(plasma_boundary_R)) + "\n")
    for i in range(len(plasma_boundary_R)):
        single_line = str(plasma_boundary_R[i]) + "\t" + str(plasma_boundary_Z[i]) + "\n"
        desired_bnd_file.write(single_line)
    desired_bnd_file.close()
    modify_xml("r0", str(center[0]))


def get_plasma_boundary(center):
    plasma_radius = GD.GOLEM_limiter_radius - np.sqrt( (GD.R_chamber_center - center[0])**2 + (GD.Z_chamber_center - center[1])**2 )
    theta = np.linspace(0, 2*np.pi, 100)
    theta = np.delete(theta, -1)

    plasma_boundary_R = plasma_radius * np.cos(theta) + center[0]
    plasma_boundary_Z = plasma_radius * np.sin(theta) + center[1]
    plasma_boundary_R = np.round_(plasma_boundary_R, 15)
    plasma_boundary_Z = np.round_(plasma_boundary_Z, 15)
    return plasma_boundary_R, plasma_boundary_Z

def get_global_params():
    """
    Function return an array of chosen global params. These are:
    0. B0
    1. beta_pol
    2. beta_tor
    3. Ip_out
    4. q_95

    Their positions in dataEqui_global_quantities.txt are:
    0. time
    2. B0
    3. beta_pol
    4. beta_tor
    6. Ip_out
    17. q_95
    """
    os.chdir(GD.path_to_GUI)
    global_params_file = open("../output/dataEqui_global_quantities.txt", "r")
    global_params_str = global_params_file.readlines()[0]
    global_params_arr = global_params_str.split()
    global_params = [float(global_params_arr[i]) for i in range(len(global_params_arr))]
    desired_global_params = [global_params[2], global_params[3], global_params[4], global_params[6], global_params[17]]
    return desired_global_params


def get_Icoils(mode, stab):
    os.chdir(GD.path_to_GUI)
    if mode == "inverse":    
        Icoils_file = open("../output/dataEqui_currents_inverse.txt", "r")
        Icoils_str = Icoils_file.readlines()[0]
        if stab:
            Icoils_arr = Icoils_str.split()[2:11]
            Icoils = [float(Icoils_arr[i]) for i in range(len(Icoils_arr))]
        else:
            Icoils = [float(Icoils_str.split()[2])]
        Icoils_file.close()
        return Icoils
    
    elif mode == "direct":
        Icoils = pd.read_csv("../input/Icoils.txt", skiprows=1, names=["currents"])
        Icoils = list(Icoils["currents"])
        return Icoils


def execute_NICE():
    os.chdir(GD.path_to_GUI)
    os.system(f"rm -rf ../output")
    os.mkdir("../output")
    os.chdir("../")
    subprocess.check_call("./nice")
    

def modify_xml(TagName, value):
    os.chdir(GD.path_to_GUI)    
    os.chdir("../input")
    params = md.parse("param.xml")
    params.getElementsByTagName(TagName)[0].firstChild.nodeValue = value
    params_w = open("param.xml", "w")
    params_w.write(params.toxml())
    params_w.close()


def set_Ip_Btor(I_p, B_tor):
    os.chdir(GD.path_to_GUI)    
    os.chdir("../input")
    Ip_Btor_file = open("Ip_B0.txt", "w")
    Ip_Btor_file.write(f"{I_p} {B_tor}")
    Ip_Btor_file.close()

def set_Isolenoid(Isolenoid):
    os.chdir(GD.path_to_GUI)    
    os.chdir("../input")
    Isolenoid_file = open("Icoils.txt", "w")
    Isolenoid_file.write(f"1\n{Isolenoid}\n")
    Isolenoid_file.close()

def set_Icoils(Icoils_arr):
    os.chdir(GD.path_to_GUI)    
    os.chdir("../input")
    coils_current_file = open("Icoils.txt", "w")
    coils_current_file.write(f"9\n{Icoils_arr[0]}\n{Icoils_arr[1]}\n{Icoils_arr[2]}\n{Icoils_arr[3]}\n{Icoils_arr[4]}\n{Icoils_arr[5]}\n{Icoils_arr[6]}\n{Icoils_arr[7]}\n{Icoils_arr[8]}\n")
    coils_current_file.close()

def stab_switch(stabilisation):
    """
    Enable or disable stabilisation coils. 

    stabilisation == True ... stab coils are enabled
    stabilisation == False ... stab coils are disabled 
    """
    os.chdir(GD.path_to_GUI)    
    os.chdir("../input")

    if stabilisation:
        # NOTE format of .txt files is quite unreadable here, but very well readable in the actual .txt files
        coils_file = open("coils.txt", "w")
        coils_file.write("9\n0.65 0.24 0.015 0.015 2\n0.65 0.22 0.015 0.015 4\n0.29 0.26 0.015 0.015 4\n0.21 0.29 0.015 0.015 2\n0.21 -0.29 0.015 0.015 2\n0.29 -0.26 0.015 0.015 4\n0.65 -0.22 0.015 0.015 4\n0.65 -0.24 0.015 0.015 2\n0.21 0.03 0.015 0.35 2\n") # last one (i.e. virtual solenoid) # was \n0.21 0.0 0.015 0.4 48\n
        coils_file.close()
        
        coils_resistance_file = open("coils_resistance.txt", "w")
        coils_resistance_file.write("9\n1.0\n1.0\n1.0\n1.0\n1.0\n1.0\n1.0\n1.0\n1.0\n")
        coils_resistance_file.close()

        coils_current_file = open("Icoils.txt", "w")
        coils_current_file.write("9\n0.0\n0.0\n0.0\n0.0\n0.0\n0.0\n0.0\n0.0\n0.0\n")
        coils_current_file.close()

        coils_voltage_file = open("voltages.txt", "w")
        coils_voltage_file.write("0    0")
        coils_voltage_file.close

        modify_xml("n_coil_group_index", "9")
        modify_xml("coil_group_index", "0 1 2 3 4 5 6 7 8")
        modify_xml("n_group_current_index", "9")        
        modify_xml("group_current_index", "0 1 2 3 4 5 6 7 8")
    
    else:
        coils_file = open("coils.txt", "w")
        coils_file.write("1\n0.21 0.03 0.015 0.35 2\n") # was "1\n0.21 0.0 0.015 0.4 48\n"
        coils_file.close()
        
        coils_resistance_file = open("coils_resistance.txt", "w")
        coils_resistance_file.write("1\n1.0\n")
        coils_resistance_file.close()

        coils_current_file = open("Icoils.txt", "w")
        coils_current_file.write("1\n0\n")
        coils_current_file.close()

        coils_voltage_file = open("voltages.txt", "w")
        coils_voltage_file.write("0    0\n")
        coils_voltage_file.close()

        modify_xml("n_coil_group_index", "1")
        modify_xml("coil_group_index", "0")
        modify_xml("n_group_current_index", "1")        
        modify_xml("group_current_index", "0")


