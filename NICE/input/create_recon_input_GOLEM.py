# FILE FOR CREATING GOLEM INPUT FOR RECON MODE

import pandas as pd
import numpy as np
import h5py

TokaMesh_GOLEM = h5py.File("/home/maty/Plocha/TokGol_2022_23/NICE_parameters/GOLEM/GOLEM_params.h5", "r")
# ---





# --- VESSEL
vessel_outer_r = np.array(TokaMesh_GOLEM["vessel/0/outline_outer_r"])
vessel_outer_z = np.array(TokaMesh_GOLEM["vessel/0/outline_outer_z"])
vessel_inner_r = np.array(TokaMesh_GOLEM["vessel/0/outline_inner_r"])
vessel_inner_z = np.array(TokaMesh_GOLEM["vessel/0/outline_inner_z"])

# ---
vessel_txt = open(r"/home/maty/Plocha/TokGol_2022_23/recon/recon_input_GOLEM/vessel.txt", "r+")

vessel_txt.write("1\n")

vessel_txt.write(str(len(vessel_inner_r))+"\n")

for i in range(len(vessel_inner_r)):
    vessel_txt.write( str(vessel_outer_r[i])+" "+str(vessel_outer_z[i])+"\n" )

vessel_txt.write("\n")
vessel_txt.write(str(len(vessel_inner_r))+"\n")

for i in range(len(vessel_inner_r)):
    vessel_txt.write( str(vessel_inner_r[i])+" "+str(vessel_inner_z[i])+"\n" )

vessel_txt.close()
# ---





# --- LIMITER
limiter_r = np.array(TokaMesh_GOLEM["limiter/r"])
limiter_z = np.array(TokaMesh_GOLEM["limiter/z"])

# ---
limiter_txt = open(r"/home/maty/Plocha/TokGol_2022_23/recon/recon_input_GOLEM/limiter.txt", "r+")

limiter_txt.write(str(len(limiter_r))+"\n")

for i in range(len(limiter_r)):
    limiter_txt.write( str(limiter_r[i])+" "+str(limiter_z[i])+"\n")

limiter_txt.close()







# --- COILS RESISTANCE
coils_resistance = []
for i in range(7): #num of coils
#    coils_resistance.append(np.array(TokaMesh_GOLEM[f"pf_active/coils/{i}/resistance"]))
    coils_resistance.append(1.0)

# ---
coils_resistance_txt = open(r"/home/maty/Plocha/TokGol_2022_23/recon/recon_input_GOLEM/coils_resistance.txt", "r+")

coils_resistance_txt.write(str(len(coils_resistance))+"\n")

for i in range(len(coils_resistance)):
    coils_resistance_txt.write(str(coils_resistance[i])+"\n")

coils_resistance_txt.close()






# --- COILS
coils_r = []
coils_z = []
coils_dr = []
coils_dz = []
coils_turns = []
for i in range(7): #num of coils
    coils_r.append( str(np.array(TokaMesh_GOLEM[f"pf_active/coils/{i}/r"])) )
    coils_z.append( str(np.array(TokaMesh_GOLEM[f"pf_active/coils/{i}/z"])) )
    coils_dr.append(str(np.array(TokaMesh_GOLEM[f"pf_active/coils/{i}/dr"])))
    coils_dz.append(str(np.array(TokaMesh_GOLEM[f"pf_active/coils/{i}/dz"])))
    coils_turns.append(str(np.array(TokaMesh_GOLEM[f"pf_active/coils/{i}/turns"])))

# ---
coils_txt = open(r"/home/maty/Plocha/TokGol_2022_23/recon/recon_input_GOLEM/coils.txt", "r+")

coils_txt.write(str(len(coils_r))+"\n")

for i in range(len(coils_r)):
    coils_txt.write(str(coils_r[i])+" "+str(coils_z[i])+" "+str(coils_dr[i])+" "+str(coils_dz[i])+" "+str(coils_turns[i])+"\n")

coils_txt.close()






# --- IRON CORE

# TO BE DONE...




# --- B MEASUREMENT PROBES (MAGNETIC FIELD COILS)
Bprobes_r = []
Bprobes_z = []
Bprobes_a = []
for i in range(16): #num of Bprobes in MHD ring
    Bprobes_r.append(str(np.array(TokaMesh_GOLEM[f"diag/MHD_ring/{i}/r"])))
    Bprobes_z.append(str(np.array(TokaMesh_GOLEM[f"diag/MHD_ring/{i}/z"])))
    a = np.array(TokaMesh_GOLEM[f"diag/MHD_ring/{i}/dr"]) * np.array(TokaMesh_GOLEM[f"diag/MHD_ring/{i}/dz"])
    Bprobes_a.append(str(a))

# ---
Bprobes_txt = open(r"/home/maty/Plocha/TokGol_2022_23/recon/recon_input_GOLEM/Bprobes.txt", "r+")

Bprobes_txt.write(str(len(Bprobes_r))+"\n")

for i in range(len(Bprobes_r)):
    Bprobes_txt.write(Bprobes_r[i]+" "+Bprobes_z[i]+" "+Bprobes_a[i]+"\n")



# --- SIGNAL ON B MEASUREMENT PROBES (comment for faster compiling)
Bmeas = []
for i in range(1): #num of Bprobes in MHD ring
    signal = pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/39699/Diagnostics/MHDring_TM/ring_{i+1}.csv")
    Bmeas.append(signal[str(signal.columns[1])][10000]) ### HERE YOU ARE TAKING A SPECIFIC VALUE OF EACH SIGNAL ###

# ---
Bmeas_txt = open(r"/home/maty/Plocha/TokGol_2022_23/recon/recon_input_GOLEM/Bprobes_meas.txt", "r+")

Bmeas_txt.write("16\n") #num of Bprobes

for i in range(len(Bmeas)):
    Bmeas_txt.write(str(Bmeas[i])+" "+"-9e+40 -9e+40\n") #-9e+40 is the absolute and relative error and its value is taken from the WEST case

Bmeas_txt.close()



TokaMesh_GOLEM.close()