import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from profiles1d_NICE import *
from profiles1d_approximated import *
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "Times New Roman"

"""File plots a figure comparing calculated (profiles1d_approximated.py) and simulated (profiles1d_NICE.py) j_tor and q radial profiles profiles. 
"""

# 1D profiles simulated by NICE
profiles1d = load_profiles1d()
profiles1d_filled = fill_profiles1d(profiles1d)
psiN = load_psiN()

# 1D profiles approximated from discharge parameters
shot   = 41980
t      = 8.0
nu     = 1
Rshift = -0.03
Zshift = 0.02
a      = calculate_plasma_radius(Rshift, Zshift)
r      = np.linspace(0, a, 101)

mu0, Ip, Btor = get_constants(shot, t)
jtor_profile = get_jtor_profile(r, a, nu, Ip)
jtor_profile = [val/1000 for val in jtor_profile] # [kA/m]
q_profile = get_q_profile(r, a, mu0, Ip, Btor, R0, nu)

# comparison graph 
fig, axs = plt.subplots(2, figsize=(6*0.7, 4*0.7))

axs[0].plot(psiN, jtor_profile, label="approximated")
# axs[0].plot(psiN, profiles1d_filled["j_tor"]/1000, label="simulated")
axs[0].scatter(psiN, profiles1d_filled["j_tor"]/1000, s=10, c="orange", marker=".", label="simulated")
axs[0].set_ylabel(r"$j_\phi$ [kA/m]", labelpad=-4)
axs[0].set_xticklabels([])
axs[0].legend(frameon=False)

axs[1].plot(psiN, q_profile, label="approximated")
# axs[1].plot(psiN, profiles1d_filled["q"], label="simulated")
axs[1].scatter(psiN, profiles1d_filled["q"], s=10, c="orange", marker=".", label="simulated")
axs[1].set_xlabel(r"$\psi/\psi_\mathrm{edge}$")
axs[1].set_ylabel(r"$q$ [-]")
axs[1].text(.0, 3.675, fr"discharge \#{shot}, $t = {t}$ ms, $\nu = {nu}$")
# axs[1].legend()

fig.tight_layout()
plt.savefig("profiles1d_comparison_nu01.pdf")
plt.show()
