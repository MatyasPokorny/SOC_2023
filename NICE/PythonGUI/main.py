import streamlit as st
from PIL import Image
from GlobalData import GD
import GUI_ENG
import GUI_CZ


def main():
    st.set_page_config(
        page_title="Tokamak GOLEM: NICE GUI", 
        layout="centered"
        )

    with st.sidebar:
        col_left, col_center, col_right = st.columns([1,2,1])
        with col_center:
            GOLEM_logo_fig = Image.open(GD.path_to_GUI + "/figs/GOLEM_logo.png")
            st.image(GOLEM_logo_fig, width=100)
        st.text("")
        st.text("")
        st.text("")
        st.title("Možnosti")
        navigaton = st.selectbox("Page", ("HOME", "Inverse", "Direct"))
        language  = st.selectbox("Language", ("Česky", "English"))


    if language == "Česky":
        if navigaton   == "HOME":    GUI_CZ.home_interface()
        elif navigaton == "Inverse": GUI_CZ.inv_interface()
        elif navigaton == "Direct":  GUI_CZ.dir_interface()
        else:                        GUI_CZ.home_interface()

        st.markdown("---")
        st.markdown("Oficiální [wiki stránky](http://golem.fjfi.cvut.cz/wiki/) tokamaku GOLEM.")
        st.markdown("[Databáze výbojů](http://golem.fjfi.cvut.cz/wiki/0/) tokamaku GOLEM.")


    elif language == "English":
        if navigaton   == "HOME":    GUI_ENG.home_interface()
        elif navigaton == "Inverse": GUI_ENG.inv_interface()
        elif navigaton == "Direct":  GUI_ENG.dir_interface()
        else:                        GUI_ENG.home_interface()

        st.markdown("---")
        st.markdown("GOLEM's official [wiki page](http://golem.fjfi.cvut.cz/wiki/).")
        st.markdown("[GOLEM's shot database](http://golem.fjfi.cvut.cz/wiki/0/).")

if __name__ == "__main__":
    main()