import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from GlobalData import GD
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "serif"

def plot_plasma_boudary(center):
    """
    Function returns a maplotlib figure for a given plasma boundary.

    center ... array of the form [R_c, Z_c] in meters
    """    
    plasma_radius = GD.GOLEM_limiter_radius - np.sqrt( (GD.R_chamber_center - center[0])**2 + (GD.Z_chamber_center - center[1])**2 )
    theta = np.linspace(0, 2*np.pi, 100)
    theta = np.delete(theta, -1)
    plasma_boundary_R = plasma_radius * np.cos(theta) + center[0]
    plasma_boundary_Z = plasma_radius * np.sin(theta) + center[1]
    chamber_R = GD.GOLEM_chamber_radius * np.cos(theta) + 0.4
    chamber_Z = GD.GOLEM_chamber_radius * np.sin(theta)
    limiter_R = GD.GOLEM_limiter_radius * np.cos(theta) + 0.4
    limiter_Z = GD.GOLEM_limiter_radius * np.sin(theta)

    fig, ax = plt.subplots(1, figsize=(8,8))
    ax.plot(limiter_R, limiter_Z, color="red", label="limiter")
    ax.plot(chamber_R, chamber_Z, color="black", label="komora")
    ax.plot(plasma_boundary_R, plasma_boundary_Z, "--", label="okraj plazmatu")
    ax.set_xlabel(r"$R \; \mathrm{[m]}$", size=15)
    ax.set_ylabel(r"$Z \; \mathrm{[m]}$", size=15)
    ax.set_xlim(0.25, 0.55)
    ax.set_ylim(-0.15, 0.15)
    ax.tick_params(labelsize=13)
    ax.grid(alpha=0.35)
    ax.set_aspect("equal")
    ax.legend(loc="upper right", frameon=False)
    
    return fig





def plot_psi(mode, chamber_only):
    os.chdir(GD.path_to_GUI)
    os.chdir("../")

    if mode == "inverse":
        desired_boundary = pd.read_csv("output/last_desired_bnd_notNICE.txt", sep="\t", skiprows=1, names=["r", "z"])
    calculated_boundary = pd.read_csv("output/plasma_bnd.txt", sep=" ", names=["r", "z"])

    mesh_coords = pd.read_csv("output/mesh_coord.txt", sep=" ", names=["r", "z"])
    psi = pd.read_csv("output/psi.txt", names=["psi"])

    mesh_r = np.array(mesh_coords["r"])
    mesh_z = np.array(mesh_coords["z"])
    psi    = np.array(psi["psi"])

    if chamber_only:
        for i in range(len(mesh_r)):
            if (mesh_r[i] > 0.6 or mesh_r[i] < 0.2) or (mesh_z[i] > 0.2 or mesh_z[i] < -0.2):
                mesh_r[i] = 1
                mesh_z[i] = 1
                psi[i] = 1000
        mesh_r = mesh_r[mesh_r != 1]
        mesh_z = mesh_z[mesh_z != 1]
        psi = psi[psi != 1000]    

    chamber = pd.read_csv("input/vessel.txt", skiprows=(2), sep=" ", names=["r", "z"])
    chamber_outer_r = chamber["r"][0:99]
    chamber_outer_z = chamber["z"][0:99]
    chamber_inner_r = chamber["r"][101:200]
    chamber_inner_z = chamber["z"][101:200]

    limiter = pd.read_csv("input/limiter.txt", skiprows=(1), sep=" ", names=["r", "z"])

    # NOTE "r" and "z" coords of coils are referring to centers, not corners
    coils = pd.read_csv("input/coils.txt", skiprows=(1), sep=" ", names=["r", "z", "dr", "dz", "turns"])
    coils_r = [coils["r"]-coils["dr"]/2, 
               coils["r"]+coils["dr"]/2, 
               coils["r"]+coils["dr"]/2, 
               coils["r"]-coils["dr"]/2, 
               coils["r"]-coils["dr"]/2]
    coils_z = [coils["z"]-coils["dz"]/2, 
               coils["z"]-coils["dz"]/2, 
               coils["z"]+coils["dz"]/2, 
               coils["z"]+coils["dz"]/2, 
               coils["z"]-coils["dz"]/2]

    # NOTE we're appending with the 1st element of each array, so that the core segments are displayed as full rectangles (same thing with the coils)
    iron_core = pd.read_csv("input/iron.txt", sep=" ", names=["r","z"])
    iron_core_r = [np.append(iron_core["r"][2:6], iron_core["r"][2]), 
                   np.append(iron_core["r"][7:11], iron_core["r"][7]), 
                   np.append(iron_core["r"][12:16], iron_core["r"][12])]
    iron_core_z = [np.append(iron_core["z"][2:6], iron_core["z"][2]), 
                   np.append(iron_core["z"][7:11], iron_core["z"][7]), 
                   np.append(iron_core["z"][12:16], iron_core["z"][12])]
    # -----
    c = 15
    fig, ax = plt.subplots(1, figsize=(c*1.1,c))
    TriContourSet = ax.tricontour(mesh_r, mesh_z, psi, levels=15)

    ax.plot(chamber_inner_r.append(pd.Series(chamber_inner_r[101])), chamber_inner_z.append(pd.Series(chamber_inner_z[101])), color="black", label="komora")
    ax.plot(chamber_outer_r.append(pd.Series(chamber_outer_r[0])), chamber_outer_z.append(pd.Series(chamber_outer_z[0])), color="black")
    ax.plot(limiter["r"].append(pd.Series(limiter["r"][0])), limiter["z"].append(pd.Series(limiter["z"][0])), color="red", label="limiter")
    ax.plot(coils_r, coils_z, color="blue")
    ax.plot([coils_r[i][0] for i in range(len(coils_r))], [coils_z[i][0] for i in range(len(coils_z))], color="blue", label="virtuální\nsolenoid")

    for i in range(len(iron_core_r)):
        ax.plot(iron_core_r[i], iron_core_z[i], color="gray")
    ax.plot(iron_core_r[1], iron_core_z[1], color="gray", label="jádro")
    if mode == "inverse":
       ax.plot(desired_boundary["r"], desired_boundary["z"], "--", linewidth=c/4, label="požadovaný\nokraj")
    ax.plot(calculated_boundary["r"], calculated_boundary["z"], "--", linewidth=c/4, label="vypočtený\nokraj")

    ax.set_xlabel("$R \; [\mathrm{m}]$", fontsize=3.5*c, labelpad=1*c)
    ax.set_ylabel("$Z \; [\mathrm{m}]$", fontsize=3.5*c, labelpad=1*c)
    ax.tick_params(axis="both", labelsize=2.7*c)
    ax.axis("equal")
    ax.grid(alpha=0.35)
    if chamber_only:
        ax.set_xlim(0.275, 0.525)
        ax.set_ylim(-0.12, 0.16)
        ax.legend(fontsize=2.5*c, loc="upper right", frameon=True)
    else:
        ax.set_xlim(0.2, 0.7)
        ax.legend(fontsize=2.5*c, loc="upper right", frameon=False)

    cbar = plt.colorbar(TriContourSet, ticks = (np.linspace(min(psi), max(psi), 7)))
    cbar.ax.set_ylabel(r"$\psi \; \mathrm{[Wb]}$", fontsize=3*c, labelpad=2*c)
    cbar.ax.tick_params(labelsize=2*c, pad=c)

    fig.tight_layout()

    return fig

