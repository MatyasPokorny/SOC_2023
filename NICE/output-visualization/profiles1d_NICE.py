import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "Times New Roman"

"""File contains functions for loading and plotting radial profiles of various quantities (including j_tor and q) simulated via NICE.
"""

def load_profiles1d():
    # parsing dataEqui_profiles_psi_rhotornorm_pressure_f_dpdpsi_fdfdpsi_jtor_q_Ne.txt and loading text as list of floats into profiles1d
    with open("../output/dataEqui_profiles_psi_rhotornorm_pressure_f_dpdpsi_fdfdpsi_jtor_q_Ne.txt") as profiles1d_file:
        profiles1d = profiles1d_file.read()
        profiles1d = [float(val) for val in profiles1d.split()]
    return profiles1d


def load_psiN():
    # parsing profiles_1d_psiN.txt and loading psiN (a possible x-axis to all 1D profile) as a list of floats 
    with open("../output/profiles_1d_psiN.txt") as psiN_file:
        psiN = psiN_file.read()
        psiN = [float(val) for val in psiN.split()]
        psiN.pop(0)
    return psiN


def fill_profiles1d(profiles1d):
    #NOTE definition of text file structure in solver_inout.cpp at line 4334

    # dealing with first values of the text file
    time       = profiles1d.pop(0)            # unclear what "time" is from src file
    n_vals     = int(profiles1d.pop(0))       # number of values in single profile
    n_profiles = int(len(profiles1d)/n_vals)  # number of profiles  

    # defining all 1D profiles present in the text file
    psi          = [] # poloidal magnetic flux
    rho_tor_norm = [] # ?
    p            = [] # pressure
    f            = [] # ? (probably f in ff')
    dp_dpsi      = [] # pressure derivative with respect to poloidal magnetic flux
    f_dp_dpsi    = [] # ?
    j_tor        = [] # current density
    q            = [] # safety factor
    Ne           = [] # ?

    # filling the profiles1d_filled list with individual 1D profiles
    profiles1d_filled = [psi, rho_tor_norm, p, f, dp_dpsi, f_dp_dpsi, j_tor, q, Ne]
    for profile in profiles1d_filled:
        for i in range(n_vals):
            profile.append(profiles1d.pop(0))
    profiles1d_filled = pd.DataFrame(profiles1d_filled).transpose()
    profiles1d_filled.columns = ["psi", "rho_tor_norm", "p", "f", "dp_dpsi", "f_dp_dpsi", "j_tor", "q", "Ne"]
    return profiles1d_filled


def plot_profile1d(profile1d):
    # creating r axis
    r = np.linspace(0, 0.085, len(profile1d))
    r *= 100 # [cm]

    # creating figure visual
    fig, ax = plt.subplots()
    ax.set_xlabel(r"$r$ [cm]")
    ax.set_ylabel(r"$q$ [-]")

    # plotting 1D profile
    ax.plot(r, profile1d)
    plt.savefig("profile1d_test.pdf")
    plt.show()


def plot_q_p_jtor(q, p, j_tor):
    # creating r axis
    r = np.linspace(0, 0.085, len(q))
    r *= 100 # [cm]

    fig, axs = plt.subplots(3, figsize=(4, 5))

    # plotting q
    axs[0].plot(r, q)
    axs[0].set_ylabel(r"$q$ [-]")
    axs[0].set_xticklabels([])

    # plotting p
    axs[1].plot(r, p)
    axs[1].set_ylabel(r"$p$ [Pa]")
    axs[1].set_xticklabels([])

    # plotting j_tor
    axs[2].plot(r, j_tor/1000)
    axs[2].set_ylabel(r"$j_\phi$ [kA/m]")
    axs[2].set_xlabel(r"$r$ [cm]")

    fig.tight_layout()
    plt.savefig("q_p_jtor_plot.pdf")
    plt.show()
