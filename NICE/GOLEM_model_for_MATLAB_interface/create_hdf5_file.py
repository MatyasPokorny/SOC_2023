# THIS IS A FILE FOR CREATING THE .hdf5 FILE FOR THE TOKAMAK GOLEM PARAMETERS

import numpy as np
import matplotlib.pyplot as plt
import h5py

# TokaMesh_GOLEM.close()  # if the code ends in an error, the .hdf5 file stays open
                         # line has to be commented when running this file for the 1st time

# -----------------------------------------------------------------------------
# DEFINING GOLEM PARAMETERS:

# 1. limiter: 
    
npoints = 100
theta = np.linspace(0, 2*np.pi, npoints)

limiter_r = 0.085 * np.cos(theta) + 0.40 #m
limiter_z = 0.085 * np.sin(theta)        #m

limiter_r = np.delete(limiter_r, -1)
limiter_z = np.delete(limiter_z, -1)

npoints = npoints - 1



# 2. vessel: 
    
conductivity_vessel = 130
 
npoints_vessel = 100
theta = np.linspace(0, 2*np.pi, npoints_vessel)
    
outline_inner_r = 0.10 * np.cos(theta) + 0.40  #m
outline_inner_z = 0.10 * np.sin(theta)         #m
outline_outer_r = 0.115 * np.cos(theta) + 0.40 #m
outline_outer_z = 0.115 * np.sin(theta)        #m

outline_inner_r = np.delete(outline_inner_r, -1)
outline_inner_z = np.delete(outline_inner_z, -1)
outline_outer_r = np.delete(outline_outer_r, -1)
outline_outer_z = np.delete(outline_outer_z, -1)

npoints_vessel = npoints_vessel - 1

# =============================================================================
# We arent accounting for copper coating for the moment.
#
# coating_inner_r = 0.1225 * np.cos(theta) + 0.40 #m
# coating_inner_z = 0.1225 * np.sin(theta)        #m
# coating_outer_r = 0.1425 * np.cos(theta) + 0.40 #m
# coating_outer_z = 0.1425 * np.sin(theta)        #m
# 
# coating_npoints = len(coating_inner_r)
# coating_conductivity = 1 / (16.78 * 10**(-9)) #S/m 
# =============================================================================



# 3. (poloidal) coils: 
#
# First four elements are the coils of the quadrupole influencing the horizontal plasma position (upper right to bottom right)
# Second four elements are the coils of the quadrupole influencing the vertical plasma position (upper right to bottom right)
# Last element of each list is attributed to the central imaginary coil that represents all the coils wound on the core

poloidal_MaxNumberOfNodes = 0 
poloidal_NumberOfCircuit = 0  
poloidal_n_coils_total = 9    # number of PF coils 
poloidal_n_psu_total = 9      # number of independent energy sources for PF coils
poloidal_useCircuits = 0      


# poloidal_r_center = [0.40+0.25, 0.40-0.11, 0.40-0.19, 0.40-0.19, 0.40-0.11, 0.40+0.25, 0.40-0.19] #m   OLD VERSION WITH ONLY 6 (resp. 7) COILS
# poloidal_z_center = [     0.24,      0.26,      0.29,     -0.29,     -0.26,     -0.24,       0.0] #m
# poloidal_N =        [        6,         2,         4,         4,         2,         6,         1] #number of turns

# poloidal_dr =       [    0.015,     0.015,     0.015,     0.015,     0.015,     0.015,     0.015] #m
# poloidal_dz =       [    0.015,     0.015,     0.015,     0.015,     0.015,     0.015,     0.400] #m


poloidal_r_center = [0.40+0.25, 0.40-0.19, 0.40-0.19, 0.40+0.25,     0.40+0.25, 0.40-0.11, 0.40-0.11, 0.40+0.25, 0.40-0.19] #m
poloidal_z_center = [     0.24,      0.29,     -0.29,     -0.24,          0.22,      0.26,     -0.26,     -0.22,       0.0] #m
poloidal_N =        [        2,         2,         2,         2,             4,         4,         4,         4,        48] #number of turns

poloidal_dr =       [    0.015,     0.015,     0.015,     0.015,         0.015,     0.015,     0.015,     0.015,     0.015] #m
poloidal_dz =       [    0.015,     0.015,     0.015,     0.015,         0.015,     0.015,     0.015,     0.015,     0.400] #m








poloidal_r = []
poloidal_z = []
for i in range(len(poloidal_r_center)):
    poloidal_r.append(poloidal_r_center[i] - poloidal_dr[i]/2)
    poloidal_z.append(poloidal_z_center[i] - poloidal_dz[i]/2)

poloidal_voltage =    [1, 1, 1, 1, 1, 1, 1]     
poloidal_resistance = [1, 1, 1, 1, 1, 1, 1] # defined in matlab file
poloidal_current =    [1, 1, 1, 1, 1, 1, 1]




# 4. iron core:
    
b_field =       [0, 0.5]        
perm_relative = [5000, 5000]    # 5000 is the relative permeability of iron.
                                # With this definition of these two variables 
                                # the perm_rel is probably not a function of
                                # b_field.

b_field = np.array(b_field).reshape( (-1,1) ) # has to be transposed, otherwise NICE will crash
perm_relative = np.array(perm_relative).reshape( (-1,1) )


# =============================================================================
# This is a wrong way of inputing the iron core. It just could serve as a 
# reference for parameters of the non-axisymmetric iron core. 
#
# 4.1. input as bottom left corner and dr, dz (not correct)
#
# core_bottom_dr = 0.1 + 0.675 + 0.25 
# core_bottom_dz = 0.26    
# core_bottom_r = 0.4 - (0.675/2 + 0.1) 
# core_bottom_z = -(0.3 + core_bottom_dz) 
# 
# core_top_dr = 0.1 + 0.675 + 0.25 
# core_top_dz = 0.26    
# core_top_r = 0.4 - (0.675/2 + 0.1) 
# core_top_z = 0.3 
# 
# core_left_dr = 0.1 
# core_left_dz = 0.6
# core_left_r = 0.4 - (0.675/2 + 0.1) 
# core_left_z = -0.3 
# 
# core_right_dr = 0.25
# core_right_dz = 0.6
# core_right_r = 0.4 + 0.675/2 
# core_right_z = -0.3 
# 
# # 4.2. input as it is the case in test-west (correct)
# iron_core_1_r = [core_bottom_r, core_bottom_r + core_bottom_dr, core_bottom_r + core_bottom_dr, core_bottom_r]
# iron_core_1_z = [core_bottom_z, core_bottom_z, core_bottom_z + core_bottom_dz, core_bottom_z + core_bottom_dz]
# 
# iron_core_2_r = [core_top_r, core_top_r + core_top_dr, core_top_r + core_top_dr, core_top_r]
# iron_core_2_z = [core_top_z, core_top_z, core_top_z + core_top_dz, core_top_z + core_top_dz]
# 
# iron_core_3_r = [core_left_r, core_left_r + core_left_dr, core_left_r + core_left_dr, core_left_r]
# iron_core_3_z = [core_left_z, core_left_z, core_left_z + core_left_dz, core_left_z + core_left_dz]
# 
# iron_core_4_r = [core_right_r, core_right_r + core_right_dr, core_right_r + core_right_dr, core_right_r]
# iron_core_4_z = [core_right_z, core_right_z, core_right_z + core_right_dz, core_right_z + core_right_dz]
# 
# iron_core_r = [iron_core_1_r, iron_core_2_r, iron_core_3_r, iron_core_4_r]
# iron_core_z = [iron_core_1_z, iron_core_2_z, iron_core_3_z, iron_core_4_z]
# =============================================================================



# 4.2. Axisymmetric model of the iron core: (source: diploma thesis T. Markovic)

core_column_r = [0.0, 0.18, 0.18, 0.0]
# core_column_z = [-0.325, -0.325, 0.325, 0.325]  # real values
core_column_z = [-0.320, -0.320, 0.320, 0.320]    # the model can't load if the segments overlap, so we make the central column smaller
disc_1_r = [0.0, 0.25, 0.25, 0.0]
disc_1_z = [0.325, 0.325, 0.575, 0.575]

disc_2_r = disc_1_r
disc_2_z = [-0.575, -0.575, -0.325, -0.325]

iron_core_T_Markovic_r = [core_column_r, disc_1_r, disc_2_r]
iron_core_T_Markovic_z = [core_column_z, disc_1_z, disc_2_z]

for i in range(len(iron_core_T_Markovic_r)): # has to be transposed, otherwise NICE will crash
    iron_core_T_Markovic_r[i] = np.array(iron_core_T_Markovic_r[i]).reshape( (-1,1) )
    iron_core_T_Markovic_z[i] = np.array(iron_core_T_Markovic_z[i]).reshape( (-1,1) )



# 6. diagnostics: 
#
# 6.1. MHD ring:  
    
MHD_ring_theta_step = np.pi/8
MHD_ring_r_center = [] #m                                                             
MHD_ring_z_center = [] #m
for i in range(16):
    MHD_ring_r_center.append( 0.0935 * np.cos(MHD_ring_theta_step*i) + 0.40 )
    MHD_ring_z_center.append( 0.0935 * np.sin(MHD_ring_theta_step*i) )
MHD_ring_dr = [0.010, 0.012, 0.012, 0.012, 0.010, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.010, 0.012, 0.012, 0.010] #length of r of cross section of coils [m]
MHD_ring_dz = [0.012, 0.014, 0.014, 0.014, 0.012, 0.014, 0.014, 0.014, 0.014, 0.014, 0.014, 0.014, 0.012, 0.014, 0.014, 0.012] #m
MHD_ring_r = [] #m
MHD_ring_z = [] #m
for i in range(16):
    MHD_ring_r.append(MHD_ring_r_center[i] - MHD_ring_dr[i]/2)
    MHD_ring_z.append(MHD_ring_z_center[i] - MHD_ring_dz[i]/2)



# 6.2. Mirnov coils: 
Mirnov_r_center = [0.40+0.093, 0.40, 0.40-0.093, 0.40] #m
Mirnov_z_center = [0, 0.093, 0, -0.093] #m

Mirnov_dr = [0.0084, 0.01, 0.0084, 0.01] #m  values are approximated                                                           
Mirnov_dz = [0.01, 0.0084, 0.01, 0.0084] #m   
                                         
Mirnov_r = [] #m
Mirnov_z = [] #m
for i in range(4):
    Mirnov_r.append(Mirnov_r_center[i] - Mirnov_dr[i]/2)
    Mirnov_z.append(Mirnov_z_center[i] - Mirnov_dz[i]/2)




# -----------------------------------------------------------------------------
# PUTTING PARAMETERS INTO A .hdf5 FILE: 
# file_name = "GOLEM_params.h5"
file_name = "GOLEM_params.h5"
path = f"/home/maty/Plocha/TokGol_2022_23/NICE_parameters/GOLEM/{file_name}"
TokaMesh_GOLEM = h5py.File(path, "w")



TokaMesh_GOLEM.create_group("limiter")
TokaMesh_GOLEM["limiter"].create_dataset("npoints", data=npoints)
TokaMesh_GOLEM["limiter"].create_dataset("r", data=limiter_r)
TokaMesh_GOLEM["limiter"].create_dataset("z", data=limiter_z)



TokaMesh_GOLEM.create_group("vessel/0")
TokaMesh_GOLEM["vessel/0"].create_dataset("conductivity", data=conductivity_vessel)
TokaMesh_GOLEM["vessel/0"].create_dataset("outline_inner_r", data=outline_inner_r)
TokaMesh_GOLEM["vessel/0"].create_dataset("outline_inner_z", data=outline_inner_z)
TokaMesh_GOLEM["vessel/0"].create_dataset("outline_outer_r", data=outline_outer_r)
TokaMesh_GOLEM["vessel/0"].create_dataset("outline_outer_z", data=outline_outer_z)



TokaMesh_GOLEM.create_group("pf_active")
TokaMesh_GOLEM.create_group("pf_active/coils")
TokaMesh_GOLEM.create_group("pf_active/supply")
TokaMesh_GOLEM["pf_active"].create_dataset("MaxNumberOfNodes", data=poloidal_MaxNumberOfNodes)
TokaMesh_GOLEM["pf_active"].create_dataset("NumberOfCircuit", data=poloidal_NumberOfCircuit)
TokaMesh_GOLEM["pf_active"].create_dataset("n_coils_total", data=poloidal_n_coils_total)
TokaMesh_GOLEM["pf_active"].create_dataset("n_psu_total", data=poloidal_n_psu_total)
TokaMesh_GOLEM["pf_active"].create_dataset("useCircuits", data=poloidal_useCircuits)
for i in range(len(poloidal_dr)):
    TokaMesh_GOLEM.create_group(f"pf_active/coils/{i}")
    TokaMesh_GOLEM.create_group(f"pf_active/supply/{i}")
for i in range(len(poloidal_dr)):
    TokaMesh_GOLEM[f"pf_active/coils/{i}"].create_dataset("dr", data=poloidal_dr[i])
    TokaMesh_GOLEM[f"pf_active/coils/{i}"].create_dataset("dz", data=poloidal_dz[i])
    TokaMesh_GOLEM[f"pf_active/coils/{i}"].create_dataset("r", data=poloidal_r_center[i])
    TokaMesh_GOLEM[f"pf_active/coils/{i}"].create_dataset("z", data=poloidal_z_center[i])
    TokaMesh_GOLEM[f"pf_active/coils/{i}"].create_dataset("turns", data=poloidal_N[i])
    TokaMesh_GOLEM[f"pf_active/coils/{i}"].create_dataset("voltage", data=poloidal_voltage)
    TokaMesh_GOLEM[f"pf_active/coils/{i}"].create_dataset("resistance", data=poloidal_resistance)
    TokaMesh_GOLEM[f"pf_active/coils/{i}"].create_dataset("current", data=poloidal_current)
for i in range(len(poloidal_dr)):
    TokaMesh_GOLEM[f"pf_active/supply/{i}"].create_dataset("voltage", data=0)
    


TokaMesh_GOLEM.create_group("PS") 
TokaMesh_GOLEM.create_group("PS/0")
TokaMesh_GOLEM["PS/0"].create_dataset("r", data=[0.4,0.45,0.45,0.4])
TokaMesh_GOLEM["PS/0"].create_dataset("z", data=[0.2,0.2,0.15,0.15])
TokaMesh_GOLEM["PS/0"].create_dataset("npoints", data=4)
TokaMesh_GOLEM["PS/0"].create_dataset("conductivity", data=300)
"""
PS ("passive structures") are not present at the tokamak GOLEM. However, the 
MATLAB script that communicates with NICE has to load some PSs, so we have to
input someting. Current PS 0 is taken from COMPASS and is deleted in matlab 
once the .h5 file is loaded.
"""



TokaMesh_GOLEM.create_group("iron_core/segment")
for i in range(3):
    TokaMesh_GOLEM.create_group(f"iron_core/segment/{i}/geometry/outline") 
for i in range(3):
    TokaMesh_GOLEM[f"iron_core/segment/{i}"].create_dataset("b_field", data=b_field)
    TokaMesh_GOLEM[f"iron_core/segment/{i}"].create_dataset("permeability_relative", data=perm_relative)
for i in range(3):
    TokaMesh_GOLEM[f"iron_core/segment/{i}/geometry/outline"].create_dataset("r", data=iron_core_T_Markovic_r[i])
    TokaMesh_GOLEM[f"iron_core/segment/{i}/geometry/outline"].create_dataset("z", data=iron_core_T_Markovic_z[i])    
"""
The current core is the axisymmetric model from the diploma thesis of T. Markovic.
"""


# =============================================================================
# We arent using magnetic diagnostics for the moment.

TokaMesh_GOLEM.create_group("diag")
TokaMesh_GOLEM.create_group("diag/Mirnov")
TokaMesh_GOLEM.create_group("diag/MHD_ring")
for i in range(len(Mirnov_r)):
    TokaMesh_GOLEM.create_group(f"diag/Mirnov/{i}")
for i in range(len(MHD_ring_r)):
    TokaMesh_GOLEM.create_group(f"diag/MHD_ring/{i}")
for i in range(len(Mirnov_r)):
    TokaMesh_GOLEM[f"diag/Mirnov/{i}"].create_dataset("dr", data=Mirnov_dr[i])
    TokaMesh_GOLEM[f"diag/Mirnov/{i}"].create_dataset("dz", data=Mirnov_dz[i])
    TokaMesh_GOLEM[f"diag/Mirnov/{i}"].create_dataset("r", data=Mirnov_r[i])
    TokaMesh_GOLEM[f"diag/Mirnov/{i}"].create_dataset("z", data=Mirnov_z[i])
    TokaMesh_GOLEM[f"diag/Mirnov/{i}"].create_dataset("turns", data=0)
    TokaMesh_GOLEM[f"diag/Mirnov/{i}"].create_dataset("resistance", data=0)
for i in range(len(MHD_ring_r)):
    TokaMesh_GOLEM[f"diag/MHD_ring/{i}"].create_dataset("dr", data=MHD_ring_dr[i])
    TokaMesh_GOLEM[f"diag/MHD_ring/{i}"].create_dataset("dz", data=MHD_ring_dz[i])
    TokaMesh_GOLEM[f"diag/MHD_ring/{i}"].create_dataset("r", data=MHD_ring_r[i])
    TokaMesh_GOLEM[f"diag/MHD_ring/{i}"].create_dataset("z", data=MHD_ring_z[i])
    TokaMesh_GOLEM[f"diag/MHD_ring/{i}"].create_dataset("turns", data=0)
    TokaMesh_GOLEM[f"diag/MHD_ring/{i}"].create_dataset("resistance", data=0)
# =============================================================================


TokaMesh_GOLEM.close()


