# FILE FOR CALCULATING B_Z IN IRON CORE VIA MEASUREMENT
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.integrate import cumulative_trapezoid, cumtrapz
plt.rcParams['text.usetex'] = True
plt.rcParams["font.family"] = "Times New Roman"
"""
Script calculates B_Z(t) in core of shot #39933 from measured voltage induced in fluxloops wound on the highest and lowest point of the iron core central column.
"""

#-------------
# DATA LOADING

horni_stab_off = pd.read_csv("http://golem.fjfi.cvut.cz/shotdir/39933/Devices/Oscilloscopes/RigolMSO5204-b/U_Loop.csv", names=["time", "voltage"])
dolni_stab_off = pd.read_csv("http://golem.fjfi.cvut.cz/shotdir/39933/Devices/Oscilloscopes/RigolMSO5204-b/U_BtCoil.csv", names=["time", "voltage"])
horni_stab_on = pd.read_csv("http://golem.fjfi.cvut.cz/shotdir/39932/Devices/Oscilloscopes/RigolMSO5204-b/U_Loop.csv", names=["time", "voltage"])
dolni_stab_on = pd.read_csv("http://golem.fjfi.cvut.cz/shotdir/39932/Devices/Oscilloscopes/RigolMSO5204-b/U_BtCoil.csv", names=["time", "voltage"])
time = np.array(horni_stab_off["time"]*1000) - 16.5 # [ms]; removing 16.5 ms offset
time = np.round(time, 1)
plasma_start, plasma_duration = 2.5, 7.2 # shot 39933; taken from GOLEM shot database website
"""
O = 2 pi r -> r = O/2pi
S = pi r^2 -> S = pi (O/2pi)^2
"""
fluxloop_len = 2.0 # m
fluxloop_surface = np.pi*(fluxloop_len/(2*np.pi))**2


#---------------------------------------
# CALCULATING B_Z IN CORE (VIA FLUXLOOPS)
"""
U = -dphi/dt -> phi = -int(U)
phi = B * S_coil -> B = phi/S_coil
"""
discharge_start_index = np.where(time==0)[0][0]
discharge_end_index = np.where(time==12)[0][0]
plasma_start_index = np.where(time==plasma_start)[0][0]
plasma_end_index = np.where(time==plasma_start+plasma_duration)[0][0]
t6_5_index = np.where(time[discharge_start_index:discharge_end_index]==6.5)[0][0]

phi = - cumtrapz(horni_stab_off["voltage"][discharge_start_index:discharge_end_index], time[discharge_start_index:discharge_end_index]/1000, initial=0) # dividing time by 1000 so that [t]=s
B_Z = phi/fluxloop_surface
print("VIA MESUREMENT: B_Z(6.5ms) = " + str(B_Z[t6_5_index]) + " T")
print("------------------------------------------")


#--------------------
# PLOTTING B_Z IN CORE
fig, ax = plt.subplots(1, figsize=(8*0.7, 5*0.7))
ax.plot(time[discharge_start_index:discharge_end_index], B_Z, label=r"$B_Z$")
ax.plot([time[plasma_start_index] for i in range(10)], np.linspace(min(B_Z), max(B_Z), 10), "--", linewidth=1, color="red", label="plazma")
ax.plot([time[plasma_end_index] for i in range(10)], np.linspace(min(B_Z), max(B_Z), 10), "--", linewidth=1, color="red")
ax.plot(np.linspace(6.5, 6.5, 10), np.linspace(min(B_Z), max(B_Z), 10), "--", linewidth=1, color="black", label=r"$t=6,5$ ms")
ax.set_title(r"\textbf{Magnetické pole v jádře; měření}")
ax.set_xlabel(r"$t$ [ms]", size=13)
ax.set_ylabel(r"$B_Z$ [T]", size=13)
ax.grid(alpha=0.25)
ax.legend(frameon=True)
fig.tight_layout()
plt.show()
# fig.savefig("/home/maty/Plocha/TokGol_2022_23/misc/figures/mag_pole_v_jadre_MERENI.pdf")


#-------------------------
# PLOTTING FLUXLOOP SIGNAL
c = 7 # general fig size
fig, axs = plt.subplots(2,1, figsize=(8/2.5*c, 5/2.5*c))
axs[0].set_title(r"\textbf{Without stabilisazion}", size=c*4)
axs[0].plot(time, horni_stab_off["voltage"], label="upper")
axs[0].plot(time, dolni_stab_off["voltage"], label="lower")
axs[0].set_xlabel("$t \; \mathrm{[ms]}$", size=c*4.5)
axs[0].set_ylabel("$U \; \mathrm{[V]}$", size=c*4.5)
axs[0].set_ylim(-13, 27)
axs[0].set_xlim(-2, 20)
axs[0].grid(alpha=0.25)
axs[0].tick_params(axis="both", labelsize=4*c)
axs[0].legend(fontsize=c*4, frameon=False, loc = "lower center")
axs[1].set_title(r"\textbf{With stabilisazion}", size=c*4)
axs[1].plot(time, horni_stab_on["voltage"], label="horní")
axs[1].plot(time, dolni_stab_on["voltage"], label="spodní")
axs[1].set_xlabel("$t \; \mathrm{[ms]}$", size=c*4.5)
axs[1].set_ylabel("$U \; \mathrm{[V]}$", size=c*4.5)
axs[1].set_ylim(-13, 27)
axs[1].set_xlim(-2, 20)
axs[1].grid(alpha=0.25)
axs[1].tick_params(axis="both", labelsize=4*c)
# axs[1].legend(fontsize=c*4, frameon=False, loc = "lower center")
fig.tight_layout(pad=8)
# plt.savefig("/home/maty/Plocha/TokGol_2022_23/misc/figures/fluxloop_signal_NOVY.pdf")
plt.savefig("fluxloops.pdf")
plt.show()


#--------------------------------------------------
# DEVIATION BETWEEN UPPER AND LOWER FLUXLOOP SIGNAL

deviation_stab_off_plasma = []
deviation_stab_off_plasma_percentage = []
horni_stab_off_plasma = horni_stab_off["voltage"][170:260] # slice 170:260 is the time when plasma was confined 
dolni_stab_off_plasma = dolni_stab_off["voltage"][170:260]
for i in range(len(horni_stab_off_plasma)):
    deviation = abs(horni_stab_off_plasma[i+170] - dolni_stab_off_plasma[i+170])
    deviation_percentage = 100 * deviation/horni_stab_off_plasma[i+170]
    deviation_stab_off_plasma.append(deviation)
    deviation_stab_off_plasma_percentage.append(deviation_percentage)    
deviation_stab_off_plasma_percentage = deviation_stab_off_plasma_percentage[4:-1] # first elements produce infinities (probably division by 0)
deviation_stab_off_plasma_avg = np.nansum(deviation_stab_off_plasma) / len(deviation_stab_off_plasma)
deviation_stab_off_plasma_max = max(deviation_stab_off_plasma)
deviation_stab_off_plasma_percentage_avg = np.nansum(deviation_stab_off_plasma_percentage) / len(deviation_stab_off_plasma_percentage)
deviation_stab_off_plasma_percentage_max = max(deviation_stab_off_plasma_percentage)
print("STAB OFF")
print("DEVIATION: avg dev: " + str(deviation_stab_off_plasma_avg) + "; max dev: " + str(deviation_stab_off_plasma_max))
print("PERCENTAGE: avg pct: " + str(deviation_stab_off_plasma_percentage_avg) + "; max pct: " + str(deviation_stab_off_plasma_percentage_max))
print("--------------------------------------")


deviation_stab_on_plasma_percentage = []
deviation_stab_on_plasma = []
horni_stab_on_plasma = horni_stab_on["voltage"][170:260]  # slice 170:260 is the time when plasma was confined
dolni_stab_on_plasma = dolni_stab_on["voltage"][170:260]
for i in range(len(horni_stab_on_plasma)):
    deviation = abs(horni_stab_on_plasma[i+170] - dolni_stab_on_plasma[i+170])
    deviation_percentage = 100 * deviation / horni_stab_on_plasma[i+170]
    deviation_stab_on_plasma.append(deviation)
    deviation_stab_on_plasma_percentage.append(deviation_percentage)
deviation_stab_on_plasma_percentage = deviation_stab_on_plasma_percentage[4:-1]
deviation_stab_on_plasma_avg = np.nansum(deviation_stab_on_plasma) / len(deviation_stab_on_plasma)
deviation_stab_on_plasma_max = max(deviation_stab_on_plasma)
deviation_stab_on_plasma_avg_pct = np.nansum(deviation_stab_on_plasma_percentage) / len(deviation_stab_on_plasma_percentage)
deviation_stab_on_plasma_max_pct = max(deviation_stab_on_plasma_percentage)
print("STAB ON")
print("DEVIATION: avg dev: " + str(deviation_stab_on_plasma_avg) + "; max dev: " + str(deviation_stab_on_plasma_max))
print("PERCENTAGE: avg pct: " + str(deviation_stab_on_plasma_avg_pct) + "; max pct: " + str(deviation_stab_on_plasma_max_pct))
print("--------------------------------------")
