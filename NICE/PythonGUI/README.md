To run the GUI, run the command `streamlit run main.py` in the `PythonGUI` dir.

The pycache dir gets created automatically.

The main problems atm:
1. Plasma bnd input via a file in inv isn't functional
2. Outputs are not very functional - user can't download any files; when any parameter is changed, output dissapears; global params missing units; fonts are not unified; it isn't looking well
3. Frontend is to be upgraded 
