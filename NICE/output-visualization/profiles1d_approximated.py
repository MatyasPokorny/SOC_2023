import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

"""File contains functions for calculating j_tor and q radial profiles. The calculations include crude approximations, applicable only for circular plasma cross-section, large aspect-ratio tokamaks. 
"""

def calculate_plasma_radius(Rshift, Zshift):
    a = l - np.sqrt(Rshift**2 + Zshift**2)
    return a


def get_constants(shot, t):
    # vacuum permeability
    mu0 = 4 * np.pi * 10**(-7) # H/m
    
    # plasma current
    Ip_all = pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shot}/Diagnostics/BasicDiagnostics/Basic/Results/Ip.csv", names=["t", "Ip"])
    Ip_all["t"] = Ip_all["t"].round(3)
    Ip_all["Ip"] = Ip_all["Ip"]*1000 # A
    Ip_index = Ip_all.loc[Ip_all["t"] == t].index[0] # finds index of time t
    Ip = Ip_all["Ip"].iloc[Ip_index]

    # toroidal magnetic field component
    Btor_all = pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shot}/Diagnostics/BasicDiagnostics/Basic/Results/Bt.csv", names=["t", "Btor"])
    Btor_all["t"] = Btor_all["t"].round(3)
    Btor_index = Btor_all.loc[Btor_all["t"] == t].index[0]
    Btor = Btor_all["Btor"].iloc[Btor_index]

    return mu0, Ip, Btor


def get_Bpol_edge(Ip, mu0, a):
    Bpol_edge = (mu0 * Ip) / (2 * np.pi * a)
    return Bpol_edge


def get_q_edge(a, R, Btor, Bpol_edge):
    q_edge = (a * Btor) / (R * Bpol_edge)
    return q_edge


def get_q_axis(q_edge, nu):
    q_axis = q_edge / (nu + 1)
    return q_axis


def get_jtor_max(Ip, a, nu):
    jtor_max = (Ip * (nu + 1)) / (np.pi * a**2)
    return jtor_max


"""Source for calculations below: DOI: 10.1007/s10894-009-9264-4 (they cite Wesson).
"""


def get_jtor_profile(r: np.array, a, nu, Ip) -> np.array:
    jtor_max = (Ip * (nu + 1)) / (np.pi * a**2)    # at or close to magnetic axis
    jtor_profile = jtor_max * (1 - (r / a)**2)**nu # valid for r <= a
    return jtor_profile


def plot_jtor_profile(r: np.array, jtor_profile: np.array, nu):
    jtor_profile /= 1000 # kA/m
    fig, ax = plt.subplots()
    ax.plot(r, jtor_profile)
    ax.set_title(f"nu = {nu}")
    ax.set_xlabel("r_plasma [m]")
    ax.set_ylabel("j_tor [kA/m]")
    plt.savefig("jtor_approximated.pdf")
    plt.show()


def get_q_profile(r: np.array, a, mu0, Ip, Btor, R0, nu):
    q_profile = (2 * np.pi * a**2 * Btor * (r / a)**2) / (mu0 * Ip * R0 * (1 - (1 - (r / a)**2)**(nu + 1)))
    return q_profile


def plot_q_profile(r: np.array, q_profile: np.array):
    fig, ax = plt.subplots()
    ax.plot(r, q_profile)
    ax.set_title(f"nu = {nu}")
    ax.set_xlabel("r_plasma [m]")
    ax.set_ylabel("q [-]")
    plt.savefig("q_approximated.pdf")
    plt.show()
