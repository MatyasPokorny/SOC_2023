# A FILE FOR PLOTTING BASIC PARAMETERS

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "Times New Roman"
"""
Usage:
    1. User defines shot session for which he wants the basic params plot at the bottom of the script. 
"""


def loadData(shotNumber):
    U_loop = pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{shotNumber}/loop_voltage", sep="\t", names = ["time", "U_loop"])
    B_tor = pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{shotNumber}/toroidal_field", sep="\t", names = ["time", "B_tor"])
    I_plasma = pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{shotNumber}/plasma_current", sep="\t", names = ["time", "I_plasma"])
    U_loop = U_loop[2500:21000]
    B_tor = B_tor[2500:21000]    
    I_plasma = I_plasma[2500:21000]
    global time
    time = U_loop["time"] * 1000
    print("test")
    return U_loop["U_loop"], B_tor["B_tor"], I_plasma["I_plasma"]


def plotBasicParameters(basicParamsArray): # [array_1, array_2, array_3 ...]; array_i = [U_loop_i, B_tor_i, I_plasma_i]
    numberOfShots = len(basicParamsArray)    
    fig, axs = plt.subplots(3, 1, figsize=(10*0.6, 11*0.6))
    for i in range(numberOfShots):
        axs[0].plot(time, basicParamsArray[i][0], label=rf"\#{shots_2[i]}")
        axs[0].set_title(r"\textbf{Napětí na závit")
        axs[0].set_xlabel(r"$t$ [ms]")
        axs[0].set_ylabel(r"$U_{loop}$ [V]")
        axs[0].grid(alpha=0.25)
        axs[0].legend(frameon=False)
        axs[1].plot(time, basicParamsArray[i][1])
        axs[1].set_title(r"\textbf{Toroidální magnetické pole}")
        axs[1].set_xlabel(r"$t$ [ms]")
        axs[1].set_ylabel(r"$B_\phi$ [T]")
        axs[1].grid(alpha=0.25)        
        axs[2].plot(time, basicParamsArray[i][2])
        axs[2].set_title(r"\textbf{Proud v plazmatu}")
        axs[2].set_xlabel(r"$t$ [ms]")
        axs[2].set_ylabel(r"$I_p$ [A]")
        axs[2].set_ylim(-150, 1900)
        axs[2].grid(alpha=0.25)
    fig.tight_layout()
    plt.show()
    #plt.savefig(f"/home/maty/Plocha/TokGol_2022_23/python/BasicParameters/BASIC_PARAMS_PLOTS/basic_params_session_{shots[0]}_{shots[-1]}.pdf")



shots = [26355,26356,26357,26358,26359,26360,26361,26362,26366,26365]
shots_2 = [26356, 26358, 26360, 26362] # for illustrative purpuses

basicParamsArray = [loadData(shots_2[i]) for i in range(len(shots_2))]
plotBasicParameters(basicParamsArray)



