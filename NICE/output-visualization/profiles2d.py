import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "Times New Roman"

"""File for loading and plotting 2D profiles (mainly psi, also B) simulated via NICE. 
"""

def load_GOLEM_data():
    global chamber_inner_r, chamber_inner_z, chamber_outer_r, chamber_outer_z, limiter
    global coils_r, coils_z
    global iron_core_r, iron_core_z

    # loading chamber and limiter data
    chamber = pd.read_csv("../input/vessel.txt", skiprows=(2), sep=" ", names=["r", "z"])
    chamber_outer_r = chamber["r"][0:99]
    chamber_outer_z = chamber["z"][0:99]
    chamber_inner_r = chamber["r"][101:200]
    chamber_inner_z = chamber["z"][101:200]
    limiter = pd.read_csv("../input/limiter.txt", skiprows=(1), sep=" ", names=["r", "z"])

    # loading PF coils data (r and z refer to centers, not corners)
    coils = pd.read_csv("../input/coils.txt", skiprows=(1), sep=" ", names=["r", "z", "dr", "dz", "turns"])
    coils_r = [coils["r"]-coils["dr"]/2, 
               coils["r"]+coils["dr"]/2, 
               coils["r"]+coils["dr"]/2, 
               coils["r"]-coils["dr"]/2, 
               coils["r"]-coils["dr"]/2]
    coils_z = [coils["z"]-coils["dz"]/2, 
               coils["z"]-coils["dz"]/2, 
               coils["z"]+coils["dz"]/2, 
               coils["z"]+coils["dz"]/2, 
               coils["z"]-coils["dz"]/2]

    # loading iron core data
    iron_core = pd.read_csv("../input/iron.txt", sep=" ", names=["r","z"])
    iron_core_r = [np.append(iron_core["r"][2:6], iron_core["r"][2]), 
                   np.append(iron_core["r"][7:11], iron_core["r"][7]), 
                   np.append(iron_core["r"][12:16], iron_core["r"][12])]
    iron_core_z = [np.append(iron_core["z"][2:6], iron_core["z"][2]), 
                   np.append(iron_core["z"][7:11], iron_core["z"][7]), 
                   np.append(iron_core["z"][12:16], iron_core["z"][12])]


def load_profiles2d():
    # parsing dataEqui_psi_br_bz_btor.txt and loading the text as a list of floats profiles2d
    with open("../output/dataEqui_psi_br_bz_btor.txt") as profiles2d_file:
        profiles2d = profiles2d_file.read()
        profiles2d = [float(val) for val in profiles2d.split()]
    return profiles2d


def fill_profiles2d(profiles2d):
    #NOTE definition of text file structure in solver_inout.cpp at line 4187

    # dealing with first values of output .txt file
    time       = profiles2d.pop(0)             # unclear what "time" is from src file
    n_nodes    = int(profiles2d.pop(0))        # number of mesh nodes in single 2D profile
    n_profiles = int(len(profiles2d)/n_nodes)  # number of 2D profiles  
    
    # defining 2D profiles
    psi  = []
    Br   = []
    Bz   = []
    Btor = []

    # filling all 2D profiles
    profiles2d_filled = [psi, Br, Bz, Btor]
    for profile in profiles2d_filled:
        for i in range(n_nodes):
            profile.append(profiles2d.pop(0))
    
    # gathering all 2D profiles in single pd.DataFrame profiles2d_filled
    profiles2d_filled = pd.DataFrame(profiles2d_filled).transpose()
    profiles2d_filled.columns = ["psi", "Br", "Bz", "Btor"]

    # getting rid of inf values in Btor profile
    for val in profiles2d_filled["Btor"]:
        if val > 1000000000000: # i.e. inf
            profiles2d_filled["Btor"] = profiles2d_filled["Btor"].replace(val, 0)

    return profiles2d_filled


def plot_profile2d(profile2d, xlim=(-0.2, 0.8), ylim=(-0.6, 0.6), text_size = 14):
    # loading mesh coordinates and converting variables to convenient types
    mesh_coords = pd.read_csv("../output/mesh_coord.txt", sep=" ", names=["r", "z"])
    mesh_r    = np.array(mesh_coords["r"])
    mesh_z    = np.array(mesh_coords["z"])
    profile2d = np.array(profile2d)

    # limiting plotted area
    for i in range(len(mesh_r)):
        if (mesh_r[i] < xlim[0] or mesh_r[i] > xlim[1]) or (mesh_z[i] < ylim[0] or mesh_z[i] > ylim[1]):
            mesh_r[i] = 1
            mesh_z[i] = 1
            profile2d[i] = 100000000 # arbitrary number not included in the profile
    mesh_r    = mesh_r[mesh_r != 1]  # this gets rid of the chosen values
    mesh_z    = mesh_z[mesh_z != 1]
    profile2d = profile2d[profile2d != 100000000] 

    # setting graph visual
    fig, ax = plt.subplots(figsize=(5, 6)) # figsize=(5, 6) for psi plot
    ax.axis("equal")
    margin = 0.1
    ax.set_xlim(xlim[0]+margin, xlim[1]-margin)
    ax.set_ylim(ylim[0]+margin, ylim[1]-margin)
    ax.set_xlabel(r"$R$ [m]", size=text_size)
    ax.set_ylabel(r"$Z$ [m]", size=text_size)
    ax.tick_params(      labelsize=text_size)

    # plotting GOLEM's cross section
    lw = 1.5
    ax.plot(chamber_inner_r.append(pd.Series(chamber_inner_r[101])), chamber_inner_z.append(pd.Series(chamber_inner_z[101])), color="black", linewidth=lw)
    ax.plot(chamber_outer_r.append(pd.Series(chamber_outer_r[0])),   chamber_outer_z.append(pd.Series(chamber_outer_z[0])),   color="black", linewidth=lw)
    ax.plot(limiter["r"].append(pd.Series(limiter["r"][0])),         limiter["z"].append(pd.Series(limiter["z"][0])),         color="red",   linewidth=lw)
    ax.plot(coils_r,                                                 coils_z,                                                 color="blue",  linewidth=lw)
    for i in range(len(iron_core_r)):
        ax.plot(iron_core_r[i],                                      iron_core_z[i],                                          color="gray",  linewidth=lw)

    # plotting plasma boundary
    try:
        desired_boundary = pd.read_csv("../output/last_desired_bnd_notNICE.txt", sep="\t", skiprows=1, names=["r", "z"])
        ax.plot(desired_boundary["r"],                              desired_boundary["z"], "--",                                             linewidth=lw, label=r"measured boundary")
    except:
        print("no desired boundary given")
    calculated_boundary = pd.read_csv("../output/plasma_bnd.txt", sep=" ", names=["r", "z"])
    ax.plot(calculated_boundary["r"],                               calculated_boundary["z"], "--",                                          linewidth=lw, label=r"simulated boundary")
    ax.legend(loc="lower right", prop={'size':  text_size-1}, frameon=True)
    ax.text(.35, .59, r"discharge \#41980", size=text_size-1)
    ax.text(.49, .52, r"$t = 9.0$ ms"     , size=text_size-1)

    # plotting 2D profile and adding a color bar
    TriContourSet = ax.tricontour(mesh_r, mesh_z, abs(profile2d)*1000, linewidths=lw/2, levels=15)
    cbar = plt.colorbar(TriContourSet)
    cbar.ax.set_ylabel(r"$|\psi|$ [mWb]", size=text_size)
    cbar.ax.tick_params(             labelsize=text_size)

    fig.tight_layout()
    plt.savefig("psi_s41980_t8.pdf")
    plt.show()



load_GOLEM_data()
profiles2d = load_profiles2d()
profiles2d_filled = fill_profiles2d(profiles2d)
Bpol_abs = np.sqrt(profiles2d_filled["Br"]**2 + profiles2d_filled["Bz"]**2)
plot_profile2d(profiles2d_filled["psi"], xlim=(-0.1, 0.75), ylim=(-0.7, 0.7)) 

#NOTE good values of axis limits:
# Btor: xlim=(0.1, 0.6), ylim=(-0.3, 0.3)
# psi:  xlim=(-0.1, 0.75), ylim=(-0.7, 0.7)
# Bpol: xlim=(0.25, 0.55), ylim=(-0.25, 0.25)
