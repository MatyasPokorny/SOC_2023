import streamlit as st
import os
import pandas as pd
import numpy as np
from PIL import Image
import functions_plotting
import functions_NICE
from GlobalData import GD




########################## HOME ##########################

def home_interface():
    st.title("NICE—grafické rozhraní pro tokamak GOLEM")
    st.markdown("""
    <div style="text-align: justify">Grafické rozhraní umožňující jednoduše a efektivně ovládat režimy <i>inverse</i> a <i>direct</i> kódu NICE na tokamaku GOLEM. Stránka <b>HOME</b> popisuje základní teorii spojenou s NICE a využití rozhaní a stánky <b>Inverse</b> a <b>Direct</b> umožňují spustit stejnojmenné režimy.</div>""", unsafe_allow_html=True)
    st.markdown("---")

    st.subheader("Úvod")
    st.markdown("""<div style="text-align: justify">Kód NICE (<i>Newton direct and Inverse Computation for Equilibrium</i>) umožňuje především vypočítat magnetohydrodynamickou (MHD) rovnováhu plazmatu. Hlavním využitím NICE je výpočet magnetické topologie při MHD rovnováze, je ale také schopen do jisté míry i simulovat samotné plazma (především vypočíst jeho polohu). NICE je sjednocením a nadstavbou tří numerických kódů (VacTH, EQUINOX, CEDRES++) a tato kompaktnost je jeho hlavní výhodou <a href="https://hal.science/hal-02734719v1/document">(B. Faugeras, 2020)</a>.</div>""", unsafe_allow_html=True)
    st.markdown(" ")
    st.markdown("""<div style="text-align: justify">NICE je schopen výpočtu MHD rovnováhy v mnoha různých režimech, viz <a href="https://hal.science/hal-02734719v1/document">(B. Faugeras, 2020)</a>. Zde jsou popsané pouze ty režimy, které byly zprovozněny pro tokamak GOLEM a které je možné využít prostřednictvím tohoto grafického rozhraní.</div>""", unsafe_allow_html=True)
    st.markdown(" ")
    st.markdown(" ")

    st.subheader("Popis režimů")
    st.markdown("""<div style="text-align: justify">Pro GOLEM jsou prozatím zprovozněny režimy <i>inverse</i> a <i>direct</i> a pracuje se na zprovoznění režimu <i>rekonstrukce</i>. Tuto notaci zde zavádíme pro jednoduchost, ale není využita v <a href="https://hal.science/hal-02734719v1/document">(B. Faugeras, 2020)</a>. Vstupní a výstupní data těchto režimů jsou následující:</div>""", unsafe_allow_html=True)
    st.markdown(" ")
    st.markdown(r"""
    - *inverse:* poloha plazmatu $\rightarrow$ proudy v poloidálních cívkách + mag. pole
    - *direct:* proudy v poloidálních cívkách $\rightarrow$ poloha plazmatu + mag. pole
    - *rekonstrukce:* magnetická měření $\rightarrow$ poloha plazmatu + proudy v poloidálních cívkách + mag. pole
    """)
    st.markdown("""<div style="text-align: justify">Před šipkou vstupní data, za šipkou výstupní data. Magnetickými měřeními jsou myšlena měření provedená pomocí např. Mirnovových cívek či fluxloopů. Tento popis režimů je zjednodušený a popisuje pouze klíčové parametry pro GOLEM; detalnější popis režimů je k nalezení ve zrojích níže.</div>""", unsafe_allow_html=True)
    st.markdown(" ")
    st.markdown(" ")

    st.subheader("Princip výpočtu")
    st.markdown("""<div style="text-align: justify">NICE provádí simulace numerickým řešením Gradovy-Shafranovovy (GS) rovnice. GS rovnice je specifický tvar rovnice MHD rovnováhy:</div>""", unsafe_allow_html=True)
    st.latex(r"\nabla p = \mathbf{j} \times \mathbf{B}.")
    st.markdown("""<div style="text-align: justify">Rovnice MHD rovnováhy popisuje stav plazmatu, při kterém sílu rozpínání plazmatu, způsobenou gradientem tlaku, kompenzuje síla externího magnetického pole a proudu v plazmatu. Rovnice popisuje rovnováhu na poloidálním průřezu, proto se uvažuje pouze poloidální složka mag. pole. GS rovnice je poté takový tvar rovnice MHD rovnováhy, který zavádí mj. funkci poloidálního magnetického toku ψ [Wb]. Tato veličina popisuje tok mag. pole v poloidálním směru a právě její tvar NICE počítá pro simulaci mag. pole.</div>""", unsafe_allow_html=True)
    st.markdown(" ")
    st.markdown(" ")

    st.subheader("Odkazy a zdroje")
    st.markdown("Následující odkazy mohou posloužit pro hlubší porozumění NICE.")
    st.markdown(""" 
    - [SOČ práce](https://gitlab.com/MatyasPokorny/SOC_2023) popisující zprovoznění NICE na GOLEMu
    - odvození GS rovnice [(D. Břeň, 2021)](https://www.dml.cz/bitstream/handle/10338.dmlcz/142921/PokrokyMFA_57-2012-2_4.pdf)
    - přehled numerických metod využitých v NICE [(B. Faugeras, 2020)](https://hal.science/hal-02734719v1/document)
    - edukační video o magnetických diagnostikách na GOLEMu *tbd*
    """)












########################## INVERSE ##########################

def inv_interface():
    st.header("Inverse")
    st.markdown("Režim *inverse* umožňuje vypočítat, jaké proudy musí procházet cívkami poloidálního magnetického pole, abychom získali zadaný okraj plazmatu. Na základě zadaného okraje plazmatu také vypočítá funkci $\psi(R, Z)$. Před spuštěním simulace je nutné zadat:")
    st.markdown(r"""
    1. proud indukovaný v plazmatu $I_p$, toroidální složku magnetického pole $B_\phi$
    2. zda využít / nevyužít stabilizační cívky
    3. souřadnice středu plazmatu $(R, Z)$ (poloměr je dopočten automaticky)
    """)

    #--------------------------------
    st.markdown("---")    
    st.subheader("Vstupní parametry")
    
    # INPUT: I_p, B_tor
    st.markdown("##### 1. Globální parametry")
    col_Ip, col_Btor = st.columns([1,1])
    with col_Ip: I_p = st.number_input("$I_p \;\; \mathrm{[A]}$", value=3600.0)
    with col_Btor: B_tor = st.number_input("$B_\phi \;\; \mathrm{[T]}$", value=0.3)
    functions_NICE.set_Ip_Btor(I_p, B_tor)

    # INPUT: STAB COILS
    st.text("")
    st.markdown("##### 2. Stabilizační cívky")
    stab = st.radio("Vyberte, zda použít / nepoužít stabilizační cívky:", ("použít", "nepoužít"))
    if stab == "nepoužít": 
        functions_NICE.stab_switch(stabilisation=False)
    elif stab == "použít": 
        functions_NICE.stab_switch(stabilisation=True)

    # INPUT: PLASMA BOUNDARY
    st.text("")
    st.markdown("##### 3. Okraj plazmatu")
    plasma_shape = st.radio("Vyberte způsob zadání okraje plazmatu:", ("zadat manuálně", "nahrát soubor"))
    st.text("")
    if plasma_shape == "nahrát soubor":
        desired_bnd_file = st.file_uploader("Nahrajte soubor s okrajem plazmatu.")        
        st.markdown("*PROZATÍM NEFUNGUJE*")
    elif plasma_shape == "zadat manuálně":
        col_cR, col_cZ = st.columns([1,1])
        with col_cR: plasma_center_R = st.number_input(r"Souřadnice $R \; \mathrm{[m]}$ středu plazmatu", step=0.001, value=0.37)
        with col_cZ: plasma_center_Z = st.number_input(r"Souřadnice $Z \; \mathrm{[m]}$ středu plazmatu", step=0.001, value=0.0)
        functions_NICE.set_plasma_boudnary([plasma_center_R, plasma_center_Z])
        # DISPLAY: PLASMA BOUNDARY
        st.text("")
        plasma_boundary_fig = functions_plotting.plot_plasma_boudary([plasma_center_R, plasma_center_Z])
        col_leftMarge, col_fig, col_rightMarge = st.columns([1,5,1])
        with col_fig: 
            st.pyplot(plasma_boundary_fig)
    

    #-----------------------
    st.markdown("---")
    st.subheader("Simulace")

    # EXECUTE NICE
    st.markdown("Po zadání požadovaných parametrů můžete spustit simulaci v režimu *inverse*:")
    col_leftMarge, col_button, col_rightMarge = st.columns([3,1,3])
    with col_button: execute_inv_button = st.button("NICE", type="primary")
    st.warning("Pokud NICE prozatím nebyl spuštěn, zobrazují se výsledky poslední simulace.")
    if execute_inv_button:
        try:
            # functions_NICE.modify_xml("algoMode", "31") CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED 
            # functions_NICE.execute_NICE()
            st.success("Simulace proběhla.")
            os.chdir(GD.path_to_GUI)

            if os.path.exists("../output/plasma_bnd.txt"):
                st.success("NICE konvergoval.")

                last_desired_bnd_file = open("../output/last_desired_bnd_notNICE.txt", "w")
                plasma_boundary_R, plasma_boundary_Z = functions_NICE.get_plasma_boundary([plasma_center_R, plasma_center_Z])
                last_desired_bnd_file.write(str(len(plasma_boundary_R)) + "\n")
                for i in range(len(plasma_boundary_R)):
                    last_desired_bnd_file.write(f"{plasma_boundary_R[i]}\t{plasma_boundary_Z[i]}\n")
                last_desired_bnd_file.close()

            else:
                st.error("NICE nekonvergoval.")
                execute_inv_button = False
        except:
            st.error("Simulace neproběhla.")
            execute_inv_button = False


    #-----------------------
    st.markdown("---")
    st.subheader("Výsledky")

    st.markdown("##### 1. Funkce $\psi$ a okraj plazmatu")
    psi_plot_cross_section = st.checkbox("Zobrazit celý poloidální průřez")
    psi_plot_chamber = st.checkbox("Zobrazit pouze průřez komoru")
    col_leftMargin, col_fig, col_right_margin = st.columns([1,2,1]) 
    if psi_plot_cross_section: 
        with col_fig: st.pyplot(functions_plotting.plot_psi(mode="inverse", chamber_only=False))
    if psi_plot_chamber: 
        with col_fig: st.pyplot(functions_plotting.plot_psi(mode="inverse", chamber_only=True))

    st.text("")
    st.text("")
    st.markdown("##### 2. Globální parametry")
    global_params = functions_NICE.get_global_params()
    st.markdown(fr"""
    - $B_\phi = {global_params[0]} \; \mathrm{{T}}$
    - $\beta_\theta = {global_params[1]}$
    - $\beta_\phi = {global_params[2]}$
    - $I_p = {global_params[3]} \; \mathrm{{A}}$
    - $q_{{95}} = {global_params[4]}$
    """)

    st.text("")
    st.text("")
    st.markdown("##### 3. Poloidální cívky")
    if stab == "nepoužít": 
        Icoils = functions_NICE.get_Icoils(mode="inverse", stab=False)
        st.markdown(fr"Proud ve virtuálním solenoidu $I = {Icoils[0]} \; \mathrm{{A}}$.")

    elif stab == "použít":
        Icoils = functions_NICE.get_Icoils(mode="inverse", stab=True)
        Icoils_dict = {
            "Cívka": [1,2,3,4,5,6,7,8,"centrální cívka"],
            "Proud [A]": Icoils
        }
        Icoils_pd_df = pd.DataFrame(Icoils_dict).set_index("Cívka")
        col_table, col_img = st.columns([1,1])
        with col_table:
            st.table(Icoils_pd_df)
        with col_img:
                PFcoils_numbers_img = Image.open(GD.path_to_GUI + "/figs/PFcoils_numbers.png")
                st.image(PFcoils_numbers_img)








########################## DIRECT ##########################

def dir_interface():
    st.header("Direct")
    st.markdown("Režim *direct* umožňuje vypočítat okraj plazmatu na základě zadaných proudů v cívkách poloidální magentického pole. Na základě zadaných proudů v cívkách také počítá funkci $\psi(R, Z)$. Před spuštěním simulace je nutné zadat:")
    st.markdown(r"""
    1. proud indukovaný v plazmatu $I_p$, toroidální složku magnetického pole $B_\phi$
    2. zda využít / nevyužít stabilizační cívky
    3. proud v poloidálních cívkách či pouze ve virtuálním centrálním solenoidu. 
    """)


    #--------------------------------
    st.markdown("---")  
    st.subheader("Vstupní parametry")

    # INPUT: I_p, B_tor
    st.markdown("##### 1. Globální parametry")
    col_Ip, col_Btor = st.columns([1,1])
    with col_Ip:  
        I_p = st.number_input(r"$I_p \;\; \mathrm{[A]}$", value=3600.0)
    with col_Btor:
        B_tor = st.number_input(r"$B_\phi \;\; \mathrm{[T]}$", value=0.3)
    functions_NICE.set_Ip_Btor(I_p, B_tor)

    # STAB COILS (enable / disable)
    st.text("")
    st.markdown("##### 2. Poloidální cívky")
    stab = st.radio("Vyberte, zda chcete použít stabilizační cívky:", ("použít", "nepoužít"))
    st.text("")
    if stab == "nepoužít": 
        functions_NICE.stab_switch(stabilisation=False)
        
        #INPUT: CENTRAL SOLENOID
        Isolenoid = st.number_input("Zadejte proud v centrální cívce.")
        functions_NICE.set_Isolenoid(Isolenoid)

    elif stab == "použít": 
        functions_NICE.stab_switch(stabilisation=True)

        # INPUT: STAB COILS
        st.markdown("Zadejte proudy v poloidálních cívkách")
        Icoils_arr = []
        Isolenoid = st.number_input("Centrální cívka")
        col_coils1, col_coils2, col_img = st.columns([1,1,2])
        with col_coils1:
            for i in range(4):
                    Icoils_i = st.number_input(f"{i+1}")
                    Icoils_arr.append(Icoils_i)                    
        with col_coils2:
            for i in range(4):
                    Icoils_i = st.number_input(f"{i+5}")
                    Icoils_arr.append(Icoils_i)
        with col_img:
            PFcoils_numbers_img = Image.open(GD.path_to_GUI + "/figs/PFcoils_numbers.png")
            st.image(PFcoils_numbers_img)    
        Icoils_arr.append(Isolenoid)
        functions_NICE.set_Icoils(Icoils_arr)
   
    
    #-----------------------
    st.markdown("---")
    st.subheader("Simulace")

    # EXECUTE NICE
    st.markdown("Po zadání požadovaných parametrů můžete spustit simulaci v režimu *direct*:")
    col_leftMarge, col_button, col_rightMarge = st.columns([3,1,3])
    with col_button: execute_dir_button = st.button("NICE", type="primary")
    st.warning("Pokud NICE prozatím nebyl spuštěn, zobrazují se výsledky poslední simulace.")    
    if execute_dir_button:
        try:
            # functions_NICE.modify_xml("algoMode", "11")  CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED
            # functions_NICE.execute_NICE()
            st.success("Simulace proběhla.")
            os.chdir(GD.path_to_GUI)
            if os.path.exists("../output/plasma_bnd.txt"):
                st.success("NICE konvergoval.")

            else:
                st.error("NICE nekonvergoval.")
                execute_dir_button = False
        except:
            st.error("Simulace neproběhla.")
            execute_dir_button = False


    #-----------------------
    st.markdown("---")
    st.subheader("Výsledky")

    st.markdown("#### 1. Funkce $\psi$ a okraj plazmatu")
    psi_plot_cross_section = st.checkbox("Zobrazit celý poloidální průřez")
    psi_plot_chamber = st.checkbox("Zobrazit pouze průřez komoru")
    col_leftMargin, col_fig, col_right_margin = st.columns([1,2,1]) 
    if psi_plot_cross_section: 
        with col_fig: st.pyplot(functions_plotting.plot_psi(mode="direct", chamber_only=False))
    if psi_plot_chamber: 
        with col_fig: st.pyplot(functions_plotting.plot_psi(mode="direct", chamber_only=True))

    st.text("")
    st.text("")
    st.markdown("#### 2. Globální parametry")
    global_params = functions_NICE.get_global_params()
    st.markdown(fr"""
    - $B_\phi = {global_params[0]} \; \mathrm{{T}}$
    - $\beta_\theta = {global_params[1]}$
    - $\beta_\phi = {global_params[2]}$
    - $I_p = {global_params[3]} \; \mathrm{{A}}$
    - $q_{{95}} = {global_params[4]}$
    """)