import os
class GD_class:
    def __init__(self):
        self.path_to_GUI = os.path.dirname(os.path.abspath(__file__))
        self.GOLEM_chamber_radius = 0.1
        self.GOLEM_limiter_radius = 0.085
        self.R_chamber_center = 0.4
        self.Z_chamber_center = 0.0
GD = GD_class()