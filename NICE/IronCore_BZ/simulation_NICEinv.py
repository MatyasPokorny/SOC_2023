# FILE FOR CALCULATING B_Z IN CORE VIA A NICE SIMULATION (VIA POLOIDAL MAG. FLUX FUNC)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.interpolate import interp2d, griddata
from scipy.optimize import curve_fit
plt.rcParams['text.usetex'] = True
plt.rcParams["font.family"] = "Times New Roman"
"""
Files mesh_coord.txt and psi.txt were produced by an inverse NICE simulation of the shot #39933 at t=6.5ms (Ip=max ... closest to Ip=3.6kA).

This script was used calculate the B_Z component of mag field in core via NICE that was compared with B_Z via measurement.
"""


def load_data():
    coords = np.loadtxt("mesh_coord.txt")
    psi = np.loadtxt("psi.txt")
    R = coords[:, 0]
    Z = coords[:, 1]
    return R, Z, psi

def plot_psi(R, Z, psi):
    plt.figure()
    plt.axis("equal")
    plt.tricontour(R, Z, psi, levels = 20)
    plt.show()

def calculate_psi_1D(R, Z, psi, plot):
    x = np.linspace(0,0.18,100)
    y = np.linspace(0,0,100)
    grid_xx, grid_yy = np.meshgrid(x, y)
    psi_1D = griddata((R, Z), psi, (grid_xx, grid_yy), method='linear')[0]
    return psi_1D

def quad_func(x, a, b, c):
    return a*(x**2) + b*x + c

def quad_func_WithoutLinear(x, a, c):
    return a*(x**2) + c

def calculate_B_Z(psi_1D, plot):
    """
    B_Z = 1/R * dpsi/dR
    """
    x = np.linspace(0,0.18,100)
    popt, pcov = curve_fit(quad_func_WithoutLinear, x, psi_1D)
    dpsi_dR = []
    for i in range(len(x)):
        # NOTE derivative calculated analytically
        dpsi_dR_i = 2*popt[0]*x[i]
        dpsi_dR.append(dpsi_dR_i)
    B_Z = []
    for i in range(len(psi_1D)):
        B_Z_i = (1/x[i]) * dpsi_dR[i]
        B_Z.append(B_Z_i)
    print("VIA NICE: B_Z(7ms) = " + str(np.average(B_Z[1:-1])) + " T")
    print(f"sigma = {np.sqrt(np.diag(pcov))}")
    return np.array(B_Z)

def calculate_MagneticFlux(B_Z):
    """
    psi = B_Z * S_coil * cos(n_surface, B_Z_dir) = B_Z * S_coil
    """
    fluxloop_len = 2.0 # m
    fluxloop_surface = np.pi*(fluxloop_len/(2*np.pi))**2
    # NOTE first value of B_Z is always inf
    B_Z_avg = np.average(B_Z[1:-1])
    MagneticFlux_avg = B_Z_avg * fluxloop_surface
    return MagneticFlux_avg

def plot_psi1D_BZ(psi_1D, B_Z):
    psi_1D, B_Z = 1000*psi_1D, 1000*B_Z
    x = np.linspace(0,0.18,100)
    popt, pcov = curve_fit(quad_func_WithoutLinear, x, psi_1D)
    fig, ax = plt.subplots(1, figsize=(8*0.8,5*0.8))
    ax.scatter(x[::5], psi_1D[::5], s=20, c="blue", marker="x", label=r"chosen $\psi$ values")
    ax.plot(x, quad_func_WithoutLinear(x, popt[0], popt[1]), color="red", label=r"$\psi(R)$ fit")
    # ax.set_title(r"\textbf{Magnetické pole v jádře; NICE simulace}")
    ax.set_xlabel(r"$R$ [m]", size=13)
    ax.set_ylabel(r"$\psi$ [mWb]", size=13)
    ax_twin = ax.twinx()
    ax_twin.plot(x, B_Z, color="green", label=r"$B_Z$")
    ax_twin.set_ylabel(r"$B_Z$ [mT]", size=13)
    ax.grid(alpha=0.25)
    ax.legend(frameon=False, loc="upper left")
    ax_twin.legend(frameon=False, loc=(0.0145, 0.78))
    fig.tight_layout()
    fig.savefig("Bz_simulation.pdf")
    plt.show()




R, Z, psi = load_data()
psi_1D = calculate_psi_1D(R, Z, psi, plot=True)
B_Z = calculate_B_Z(psi_1D, plot=False)
plot_psi1D_BZ(psi_1D, B_Z)
