import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from math import floor
plt.rcParams['text.usetex'] = True    
plt.rcParams["font.family"] = "Times New Roman"

"""
File for analysing data from the 4.5.23 GOLEM session.

First shot: #41697 
Last shot:  #41705

Input Ucd was gradually incresed in 50V steps from 350V to 700V. Last shot was a reproduction of 700V with lower working gas pressure.
During the session we were reproducing the shot #27466 for the sake of its comparaison with NICE simulation. For this shot, we have METIS computed 1D profiles.

---------------------

Shot #41705 was the most successful.

Plasma start: 1.61ms 
Plasma end:   8.11ms
"""


def moving_average(array: pd.Series, time_step: float) -> pd.Series:
    """
    INPUT
    The input array must be the full 60 ms record of an arbitrary quantity; time_step has to be inputed in milliseconds.
    
    OUTPUT
    Function outputs averaged array in the form of a Pandas series indexed by 60 ms.
    """
    n_dataPoints = floor(60/time_step)
    array_avg = []
    for i in range(n_dataPoints):
        try:
            dataPoint_i = sum(array[i*time_step:(i+1)*time_step])/len(array[i*time_step:(i+1)*time_step])
            array_avg.append(dataPoint_i)
        except:
            array_avg.append(None)
    time_index = np.linspace(0, 60, len(array_avg))
    array_avg = pd.Series(array_avg)
    array_avg.index = time_index
    return array_avg


def load_all_data(shotNumber: int, plotData: bool) -> tuple[pd.Series, pd.Series, pd.Series, pd.Series]:
    """
    All quantities are loded in the form of a Pandas series and indexed by time [ms].
    """
    data1 = pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/Devices/Oscilloscopes/RigolDS1104Z-a/Bt_Ecd_currents/Trigger.csv", names = ["time", "trigger"])
    data2 = pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/Devices/Oscilloscopes/RigolDS1104Z-a/Bt_Ecd_currents/U_Bt.csv",    names = ["time", "U_Btor"])
    data3 = pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/Devices/Oscilloscopes/RigolDS1104Z-a/Bt_Ecd_currents/U_loop.csv",  names = ["time", "U_loop"])
    # total current in primary transformator coils (oscilloscope measures voltage; conversion 5mV=1A)
    data4 = pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/Devices/Oscilloscopes/RigolDS1104Z-a/Bt_Ecd_currents/U_Ecd.csv",   names = ["time", "U_cd"]) # [V]
    trigger = data1["trigger"]
    U_Btor  = data2["U_Btor"]
    U_loop  = data3["U_loop"]
    U_cd    = data4["U_cd"]
    trigger.index, U_Btor.index, U_loop.index, U_cd.index = data1["time"]*1000, data2["time"]*1000, data3["time"]*1000, data4["time"]*1000 # [ms]
    print(f"shot {shotNumber} data loaded")
    if plotData:
            fig, ax = plt.subplots(1,1, figsize=(5,5))
            ax.plot(trigger, label="trigger")
            ax.plot(U_Btor,  label="U_Btor")
            ax.plot(U_loop,  label="U_loop")
            ax.plot(U_cd,    label="U_cd")
            plt.legend()
            plt.show()
    return trigger, U_Btor, U_loop, U_cd


def load_single_Ucd(shotNumber: int) -> pd.Series:
    """
    U_cd [V] is loded in the form of a Pandas series and indexed by time [ms].
    """
    data = pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/Devices/Oscilloscopes/RigolDS1104Z-a/Bt_Ecd_currents/U_Ecd.csv",   names = ["time", "U_cd"]) # [V]
    U_cd = data["U_cd"]
    U_cd.index = (data["time"]*1000) - 5 # [ms]; -5ms due to oscilloscope trigger being 5ms before discharge initiation
    print(f"Ucd during shot {shotNumber} loaded")
    return U_cd


def load_Ucd_array(shot_init: int, n_shots: int, Ucd_input_init: int) -> tuple[list[int], list[pd.Series]]:
    """
    INPUT
    Outputs a list (not pd.Series) of Ucd(t) in the form [Ucd_{shot_init}, Ucd_{shot_init+1}, Ucd_{shot_init+1}, ... Ucd_{shot_init+n_shots}]. 
    Ucd_input_init is the Ucd value (single value, not time evolution) given to GOLEM as input for the shot #shot_init. 
    
    OUTPUT
    Outputs two lists; first a list containing Ucd values given as input to GOLEM and second see INPUT.
    """
    Ucd_input_arr = []
    Ucd_arr = []
    for i in range(n_shots):
        U_cd = load_single_Ucd(shot_init+i)
        Ucd_arr.append(U_cd)
        Ucd_input_arr.append(Ucd_input_init + 50*i)
    return Ucd_input_arr, Ucd_arr


def plot_single_Icd(U_cd: pd.Series, Ucd_input: int, shotNumber: int, **kwargs: dict[float]) -> None:
    """
    Plots Icd(t) of chosen shot. Ucd_input and shotNumber have to be inputted, so they can be displayed in the graph.
    One can add plasmaStart and plasmaEnd as optional arguments: args = [plasmaStart, plasmaEnd].
    """
    I_cd = 1/0.005 * U_cd # 5mV=1A
    I_cd /= 1000 # [kA]
    fig, ax = plt.subplots(1,1, figsize=(7, 4))
    ax.plot(I_cd)
    # ax.set_title(fr"\begin{{center}} \textbf{{Celkový proud v primárních cívkách transformátoru}}\\(výboj \#{shotNumber}; $U_{{\mathrm{{CD}}}} = {Ucd_input} \; \mathrm{{V}}$) \end{{center}}", size = 14, pad=25)
    ax.set_xlabel(r"$t \; \mathrm{[ms]}$",  size = 15)
    ax.set_ylabel(r"$I_\mathrm{CD} \; \mathrm{[kA]}$",   size = 15)
    ax.set_xlim(-1, 11)
    ax.set_ylim(-.030, .600)
    try:
        ax.plot([kwargs["plasmaStart"] for i in range(100)], np.linspace(0, 600, 100), "--", color="red", label="plasma")
        ax.plot([kwargs["plasmaEnd"]   for i in range(100)], np.linspace(0, 600, 100), "--", color="red")
    except:
        print("plasmaStart, plasmaEnd not provided")
    
    #NOTE line below added for SOČ illustration purposes
    ax.plot([5 for i in range(100)], np.linspace(0, 600, 100), "--", color="black", label=r"\textit{t} = 5,0 ms")
    
    ax.grid(alpha=0.25)
    ax.legend(frameon=False, prop={'size': 12}, loc="lower right")
    fig.tight_layout()
    # plt.savefig(f"figs/Icd_{shotNumber}.pdf")
    # plt.savefig(f"I_prim-transf-coils.pdf")
    plt.show()


def plot_Icd_array(Ucd_input_arr: list[int], Ucd_arr: list[pd.Series]) -> None:
    """
    Plots Icd(t) for each Ucd(t) in Ucd_arr in a single graph.  
    """
    Icd_arr = [Ucd_arr[i]/0.005 for i in range(len(Ucd_arr))]
    fig, ax = plt.subplots(1,1, figsize=(10,7))
    for i in range(len(Ucd_arr)):
        ax.plot(Icd_arr[i], label=f"{Ucd_input_arr[i]} V")
    ax.set_title(r"\textbf{Celkový proud v primárních transf. cívkách}",          size = 15)
    ax.set_xlabel(r"$t \; \mathrm{[ms]}$",                                        size = 15)
    ax.set_ylabel(r"$I \; \mathrm{[A]}$",                                         size = 15)
    ax.grid(alpha=0.25)
    ax.legend()
    plt.savefig("figs/Icd_all.pdf")
    plt.show()


if __name__ == "__main__":
    shot_700V = 41705       # Ucd = 700V + lower pressure
    Ucd_input_700V = 700

    Ucd_700V = load_single_Ucd(shot_700V)
    Ucd_700V_avg = moving_average(Ucd_700V, 0.5)

    #NOTE Ip(5.0ms) = max ~ 2.65kA
    Icd_700V = 1/0.005 * Ucd_700V
    print(f"Icd(5.0ms) = {Icd_700V[5.0]} A")

    plot_single_Icd(Ucd_700V, Ucd_input_700V, shot_700V, plasmaStart=1.61, plasmaEnd=8.11)
