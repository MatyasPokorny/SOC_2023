# FILE TO DETERMINE PLASMA POSITION BY SIGNAL IN MIRNOV COILS (plasma is approximated as an infinitely long narrow cylinder)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
from scipy import integrate
plt.rcParams['text.usetex'] = True    
plt.rcParams["font.family"] = "Times New Roman"
"""
Plasma position calcualtion:
    1. integrate MC voltages to obtain measured magnetic (B) field
    2. subtract vacuum B field from MC B field
    3. apply formula for radial / vertical shift calculation
    4. plot results

Usage:
    1. input shotNumber and vacuumShotNumber variables

! This is the more stable version of the MirnovCoils.py files. It doesn't produce the most usable plots, but the calculations should work well.

! Works for recent (29.3.23) shot sessions.
"""

# ------------
# INPUT PARAMS
shotNumber = 39125
vacuumShotNumber = 39109
distanceFromChamberCenterMC = 0.093 # m
effectiveAreaMC = 0.0038 # m^2
limiterDiameter = 0.085 # m


#-------
# SCRIPT

def applyMovingAverage(array, numberOfDataPoints):
    step = len(array)/numberOfDataPoints
    step = int(step)
    arrayAveraged = []
    for i in range(numberOfDataPoints):
        dataPoint = sum(array[i*step:(i+1)*step]) / len(array[i*step:(i+1)*step])
        arrayAveraged.append(dataPoint)
    return arrayAveraged

def loadData(shotNumber, vacuumShotNumber):
    signalMC1 = pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(shotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc1.csv", names=["time", "voltage"])
    signalMC5 = pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(shotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc5.csv", names=["time", "voltage"])
    signalMC9 = pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(shotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc9.csv", names=["time", "voltage"])
    signalMC13 = pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(shotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc13.csv", names=["time", "voltage"])
    vacuumSignalMC1 = pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(vacuumShotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc1.csv", names=["time", "voltage"])
    vacuumSignalMC5 = pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(vacuumShotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc5.csv", names=["time", "voltage"])
    vacuumSignalMC9 = pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(vacuumShotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc9.csv", names=["time", "voltage"])
    vacuumSignalMC13 = pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(vacuumShotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc13.csv", names=["time", "voltage"])
    time = signalMC1["time"] * 1000 # milliseconds    
    
    plasmaStart = float(pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/Diagnostics/BasicDiagnostics/Results/t_plasma_start").columns[0])
    plasmaEnd = float(pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/Diagnostics/BasicDiagnostics/Results/t_plasma_end").columns[0])
    print("plasmaStart: " + str(plasmaStart) + "; plasmaEnd: " + str(plasmaEnd))    
    plasmaStartIndex = pd.DataFrame(time[time == plasmaStart].index)
    plasmaEndIndex = pd.DataFrame(time[time == plasmaEnd].index)
    
    i = 0
    while plasmaStartIndex.empty == True: # sometimes the 2 lines above produce an empty DataFrame, this while cycle fixes it
        plasmaStartIndex = pd.DataFrame(time[time == plasmaStart + (0.001 * i)].index)
        i += 1
    i = 0
    while plasmaEndIndex.empty == True:
        plasmaEndIndex = pd.DataFrame(time[time == plasmaEnd - (0.001 * i)].index)
        i += 1

    plasmaStartIndex = plasmaStartIndex[0][0]
    plasmaEndIndex = plasmaEndIndex[0][0]
    
    voltageMC1 =  signalMC1["voltage"][plasmaStartIndex:plasmaEndIndex]
    voltageMC5 =  signalMC5["voltage"][plasmaStartIndex:plasmaEndIndex]
    voltageMC9 =  signalMC9["voltage"][plasmaStartIndex:plasmaEndIndex]
    voltageMC13 = signalMC13["voltage"][plasmaStartIndex:plasmaEndIndex]
    vacuumVoltageMC1 =  vacuumSignalMC1["voltage"][plasmaStartIndex:plasmaEndIndex]
    vacuumVoltageMC5 =  vacuumSignalMC5["voltage"][plasmaStartIndex:plasmaEndIndex]
    vacuumVoltageMC9 =  vacuumSignalMC9["voltage"][plasmaStartIndex:plasmaEndIndex]
    vacuumVoltageMC13 = vacuumSignalMC13["voltage"][plasmaStartIndex:plasmaEndIndex]
    time = time[plasmaStartIndex:plasmaEndIndex]

    allVacuumVoltages = [vacuumVoltageMC1, vacuumVoltageMC5, vacuumVoltageMC9, vacuumVoltageMC13]
    allVoltages =  [voltageMC1, voltageMC5, voltageMC9, voltageMC13]
    return allVoltages, allVacuumVoltages, time

def eliminateVacuumSignal(allMagneticFields, allVacuumVoltages, time):    
    allMagneticFields_elim = []
    for i in range(len(allMagneticFields)):
        currentVacuumMagneticField = 1/effectiveAreaMC * integrate.cumtrapz(allVacuumVoltages[i], time/1000, initial=0)
        currentMagneticField_elim = allMagneticFields[i] - currentVacuumMagneticField
        allMagneticFields_elim.append(currentMagneticField_elim)
    return allMagneticFields_elim

def calculateMagneticField(allVoltages, allVacuumVoltages, time):
    allMagneticFields = []
    for i in range(len(allVoltages)):
        currentMagneticField = 1/effectiveAreaMC * integrate.cumtrapz(allVoltages[i], time/1000, initial=0)
        allMagneticFields.append(currentMagneticField)
    allMagneticFields_elim = eliminateVacuumSignal(allMagneticFields, allVacuumVoltages, time)
    return allMagneticFields_elim        

def calculateCenerShifts(allMagneticFields):
    radialShift = []
    verticalShift = []
    for i in range(len(allMagneticFields[0])):
        currentRadialShift = distanceFromChamberCenterMC * (allMagneticFields[0][i] - allMagneticFields[2][i]) / (allMagneticFields[0][i] + allMagneticFields[2][i])            
        currentVerticalShift = distanceFromChamberCenterMC * (allMagneticFields[1][i] - allMagneticFields[3][i]) / (allMagneticFields[1][i] + allMagneticFields[3][i])
        radialShift.append(currentRadialShift)
        verticalShift.append(currentVerticalShift)
    return radialShift, verticalShift

def calculatePlasmaRadius(radialShift, verticalShift):
    plasmaRadius = []
    for i in range(len(radialShift)):
        currentPlasmaRadius = limiterDiameter - np.sqrt(radialShift[i]**2 + verticalShift[i]**2)
        plasmaRadius.append(currentPlasmaRadius)
    return plasmaRadius

def plotCenterShifts(radialShift, verticalShift, time, plotPlasmaRadus): # plotPlasmaRadius is a bool stating if func should plot plasma radius
    fig, axs = plt.subplots(2,1, figsize=(12,8))
    axs[0].plot(time, radialShift)
    axs[0].set_ylim(-0.07, 0.07)
    axs[0].set_title(r"$\mathbf{Radial \; shift}$", size = 20)
    axs[0].set_ylabel(r"$\Delta r \; \mathrm{[m]}$", size = 20)
    axs[0].set_xlabel(r"$t \; \mathrm{[ms]}$", size = 20)
    axs[0].tick_params(axis = "both", labelsize = 15)
    axs[0].grid(alpha = 0.5)
    axs[1].plot(time, verticalShift)    
    axs[1].set_ylim(-0.07, 0.07)
    axs[1].set_title(r"$\mathbf{Vertical \; shift}$", size = 20)
    axs[1].set_ylabel(r"$\Delta z \; \mathrm{[m]}$", size = 20)
    axs[1].set_xlabel(r"$t \; \mathrm{[ms]}$", size = 20)
    axs[1].tick_params(axis = "both", labelsize = 15)
    axs[1].grid(alpha = 0.5)

    fig.tight_layout()
    if plotPlasmaRadus == True:
        plasmaRadius = calculatePlasmaRadius(radialShift, verticalShift)
        fig, ax = plt.subplots(1,1, figsize=(10,4))
        ax.plot(time, plasmaRadius)
        ax.set_title(r"$\mathbf{Plasma \; radius}$", size = 20)
        ax.set_xlabel(r"$t \; \mathrm{[ms]}$", size = 20)
        ax.set_ylabel(r"$a \; \mathrm{[m]}$", size = 20)
        ax.set_ylim(0.00, 0.10)
        ax.tick_params(axis = "both", labelsize = 15)
        ax.grid(alpha=0.5)
        fig.tight_layout()
    plt.show()

def plotPlasmaBoundary(radialShift, verticalShift, time, numberOfBoundaries):
    time = applyMovingAverage(time, numberOfBoundaries)
    radialShift = applyMovingAverage(radialShift, numberOfBoundaries)      
    verticalShift = applyMovingAverage(verticalShift, numberOfBoundaries)
    plasmaRadius = calculatePlasmaRadius(radialShift, verticalShift)
    theta = np.linspace(0, 2*np.pi, 100) # 100 is the number of data points in one boundary
    limiter_r = limiterDiameter * np.cos(theta) + 0.4
    limiter_z = limiterDiameter * np.sin(theta) 
    plasmaBoundary_r = []
    plasmaBoundary_z = []
    for i in range(numberOfBoundaries):
        plasmaBoundary_r.append(plasmaRadius[i] * np.cos(theta) + radialShift[i] + 0.4)    
        plasmaBoundary_z.append(plasmaRadius[i] * np.sin(theta) + verticalShift[i])    
            
    fig, ax = plt.subplots(1,1, figsize=(7,6))
    ax.plot(limiter_r, limiter_z, color="blue", linewidth=6)
    for i in range(numberOfBoundaries):
        if plasmaRadius[i] < 0:
            continue
        ax.scatter(radialShift[i] + 0.4, verticalShift[i], s = 100, marker = "x", color = (1-i/numberOfBoundaries,0.3,1-i/numberOfBoundaries))
        ax.plot(plasmaBoundary_r[i], plasmaBoundary_z[i], label=f"${round(time[i], 1)} \; \mathrm{{ms}}$", linewidth = 4, color = (1-i/numberOfBoundaries,0.3,1-i/numberOfBoundaries))
        ax.set_title(fr"\textbf{{Plasma boundary shot {str(shotNumber)}}}", size = 20)
        ax.set_xlabel(r"$R \; \mathrm{[m]}$", size = 20)
        ax.set_ylabel(r"$Z \; \mathrm{[m]}$", size = 20)
        ax.tick_params(axis = "both", labelsize = 15)
        ax.axis("equal")
    ax.grid(alpha = 0.25)
    ax.legend(loc="lower right", prop={'size': 13})
    fig.tight_layout()
    # plt.savefig(f"/home/maty/Plocha/TokGol_2022_23/python/plasmaPosition_figures/PLASMA_BOUNDARY/plasma_boundary_shot_{shotNumber}.pdf")
    plt.show()


allVoltages, allVacuumVoltages, time = loadData(shotNumber, vacuumShotNumber)
print("test1")
allMagneticFields = calculateMagneticField(allVoltages, allVacuumVoltages, time)
print("test2")
radialShift, verticalShift = calculateCenerShifts(allMagneticFields)
print("test3")
plotCenterShifts(radialShift, verticalShift, time, plotPlasmaRadus=False)
print("test4")
plotPlasmaBoundary(radialShift, verticalShift, time, 11)
