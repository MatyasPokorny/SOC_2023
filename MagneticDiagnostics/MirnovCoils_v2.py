# FILE TO DETERMINE PLASMA POSITION BY SIGNAL IN MIRNOV COILS (INFINITELY LONG CONDUCTEUR APPROXIMATION)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
from scipy import integrate
import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning) # not ideal...
plt.rcParams['text.usetex'] = True    
plt.rcParams["font.family"] = "Times New Roman"
"""
TO BE DONE:
    1. add comments to functions
    2. create data loading func
    3. create calculatePlasmaRadius func

Plasma position calculation:
    1. integrate MC voltages to obtain measured magnetic (B) field
    2. subtract vacuum B field from MC B field
    3. apply formula for radial / vertical shift calculation
    4. plot results

Usage:
    1. rather use the MirnovCoils_v1.py file

! Works only for integer time values.

! Works only for the old shot sessions analysed during SOČ 2022/23.

! Is very unstable.

! INPUT PARAMS at the bottom.
"""



def applyMovingAverage(array, numberOfDataPoints):
    step = len(array)/numberOfDataPoints
    step = int(step)
    arrayAveraged = []
    for i in range(numberOfDataPoints):
        dataPoint = sum(array[i*step:(i+1)*step]) / len(array[i*step:(i+1)*step])
        arrayAveraged.append(dataPoint)
    return arrayAveraged


def loadData(shotNumber, vacuumShotNumber):
    
    # ------------------- NEW -----------------------
    # signalMC1 =        pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(shotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc1.csv", names=["time", "voltage"])
    # signalMC5 =        pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(shotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc5.csv", names=["time", "voltage"])
    # signalMC9 =        pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(shotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc9.csv", names=["time", "voltage"])
    # signalMC13 =       pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(shotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc13.csv", names=["time", "voltage"])
    # vacuumSignalMC1 =  pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(vacuumShotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc1.csv", names=["time", "voltage"])
    # vacuumSignalMC5 =  pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(vacuumShotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc5.csv", names=["time", "voltage"])
    # vacuumSignalMC9 =  pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(vacuumShotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc9.csv", names=["time", "voltage"])
    # vacuumSignalMC13 = pd.read_csv(f"https://golem.fjfi.cvut.cz/shots/{str(vacuumShotNumber)}/Diagnostics/LimiterMirnovCoils/U_mc13.csv", names=["time", "voltage"])
    # print(signalMC1["voltage"])

    # ------------------- OLD -----------------------
    signalMC1 =        pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{str(shotNumber)}/mirnov_1",        sep="\t", names=["time", "voltage"])
    signalMC5 =        pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{str(shotNumber)}/mirnov_5",        sep="\t", names=["time", "voltage"])
    signalMC9 =        pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{str(shotNumber)}/mirnov_9",        sep="\t", names=["time", "voltage"])
    signalMC13 =       pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{str(shotNumber)}/mirnov_13",       sep="\t", names=["time", "voltage"])
    vacuumSignalMC1 =  pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{str(vacuumShotNumber)}/mirnov_1",  sep="\t", names=["time", "voltage"])
    vacuumSignalMC5 =  pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{str(vacuumShotNumber)}/mirnov_5",  sep="\t", names=["time", "voltage"])
    vacuumSignalMC9 =  pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{str(vacuumShotNumber)}/mirnov_9",  sep="\t", names=["time", "voltage"])
    vacuumSignalMC13 = pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{str(vacuumShotNumber)}/mirnov_13", sep="\t", names=["time", "voltage"])

    global time
    time = signalMC1["time"] * 1000 # milliseconds
        
    global plasmaStart
    global plasmaEnd
    # ------------------- #39932 -----------------------
    # plasmaStart = 2.53
    # plasmaEnd = 11.33  
    # ------------------- OLD -----------------------    
    plasmaStart = (float(pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{shotNumber}/plasma_start").columns[0]) * 1000) 
    plasmaEnd = (float(pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{shotNumber}/plasma_end").columns[0]) * 1000)   
    print(f"BEFORE ROUNDING: plasmaStart: {plasmaStart} ms; plasmaEnd: {plasmaEnd}")
    plasmaStart = math.floor(plasmaStart)
    plasmaEnd = math.ceil(plasmaEnd)
    print(f"AFTER ROUNDING: plasmaStart: {plasmaStart} ms; plasmaEnd: {plasmaEnd}")
    plasmaStartIndex = plasmaStart * 1000
    plasmaEndIndex = plasmaEnd * 1000
    
    voltageMC1        = signalMC1["voltage"][plasmaStartIndex:plasmaEndIndex]
    voltageMC5        = signalMC5["voltage"][plasmaStartIndex:plasmaEndIndex]
    voltageMC9        = signalMC9["voltage"][plasmaStartIndex:plasmaEndIndex]
    voltageMC13       = signalMC13["voltage"][plasmaStartIndex:plasmaEndIndex]
    vacuumVoltageMC1  = vacuumSignalMC1["voltage"][plasmaStartIndex:plasmaEndIndex]
    vacuumVoltageMC5  = vacuumSignalMC5["voltage"][plasmaStartIndex:plasmaEndIndex]
    vacuumVoltageMC9  = vacuumSignalMC9["voltage"][plasmaStartIndex:plasmaEndIndex]
    vacuumVoltageMC13 = vacuumSignalMC13["voltage"][plasmaStartIndex:plasmaEndIndex]
    time              = time[plasmaStartIndex:plasmaEndIndex]

    allVacuumVoltages = [vacuumVoltageMC1, vacuumVoltageMC5, vacuumVoltageMC9, vacuumVoltageMC13]
    allVoltages =  [voltageMC1, voltageMC5, voltageMC9, voltageMC13]
   
    return allVoltages, allVacuumVoltages



def eliminateVacuumSignal(allMagneticFields, allVacuumVoltages):    
    allMagneticFields_elim = []
    for i in range(len(allMagneticFields)):
        currentVacuumMagneticField = 1/effectiveAreaMC * integrate.cumtrapz(allVacuumVoltages[i], time/1000, initial=0)
        currentMagneticField_elim = allMagneticFields[i] - currentVacuumMagneticField
        allMagneticFields_elim.append(currentMagneticField_elim)
    return allMagneticFields_elim



def calculateMagneticField(allVoltages, allVacuumVoltages):
    allMagneticFields = []
    for i in range(len(allVoltages)):
        print(len(allVoltages[i]), len(time))
        currentMagneticField = 1/effectiveAreaMC * integrate.cumtrapz(allVoltages[i], time/1000, initial=0)
        allMagneticFields.append(currentMagneticField)
    allMagneticFields_elim = eliminateVacuumSignal(allMagneticFields, allVacuumVoltages)
    return allMagneticFields_elim        



def calculateCenerShifts(allMagneticFields):
    radialShift = []
    verticalShift = []
    for i in range(len(allMagneticFields[0])):
        currentRadialShift = distanceFromChamberCenterMC * (allMagneticFields[0][i] - allMagneticFields[2][i]) / (allMagneticFields[0][i] + allMagneticFields[2][i])            
        currentVerticalShift = distanceFromChamberCenterMC * (allMagneticFields[1][i] - allMagneticFields[3][i]) / (allMagneticFields[1][i] + allMagneticFields[3][i])
        radialShift.append(currentRadialShift)
        verticalShift.append(currentVerticalShift)
    return radialShift, verticalShift
    


def calculatePlasmaRadius(radialShift, verticalShift):
    plasmaRadius = []
    for i in range(len(radialShift)):
        currentPlasmaRadius = limiterDiameter - np.sqrt(radialShift[i]**2 + verticalShift[i]**2)
        plasmaRadius.append(currentPlasmaRadius)
    return plasmaRadius



def plotCenterShifts(radialShift, verticalShift, plotPlasmaRadus): # plotPlasmaRadius is a bool stating if func should plot plasma radius
    fig, axs = plt.subplots(2,1, figsize=(12,8))
    
    axs[0].plot(time, radialShift)
    axs[0].set_ylim(-0.07, 0.07)
    axs[0].set_title(r"$\mathbf{Radial \; shift}$", size = 20)
    axs[0].set_ylabel(r"$\Delta r \; \mathrm{[m]}$", size = 20)
    axs[0].set_xlabel(r"$t \; \mathrm{[ms]}$", size = 20)
    axs[0].tick_params(axis = "both", labelsize = 15)
    axs[0].grid(alpha = 0.5)
    
    axs[1].plot(time, verticalShift)    
    axs[1].set_ylim(-0.07, 0.07)
    axs[1].set_title(r"$\mathbf{Vertical \; shift}$", size = 20)
    axs[1].set_ylabel(r"$\Delta z \; \mathrm{[m]}$", size = 20)
    axs[1].set_xlabel(r"$t \; \mathrm{[ms]}$", size = 20)
    axs[1].tick_params(axis = "both", labelsize = 15)
    axs[1].grid(alpha = 0.5)

    fig.tight_layout()
    
    if plotPlasmaRadus == True:
        plasmaRadius = calculatePlasmaRadius(radialShift, verticalShift)
        
        fig, ax = plt.subplots(1,1, figsize=(10,4))
    
        ax.plot(time, plasmaRadius)
        ax.set_title(r"$\mathbf{Plasma \; radius}$", size = 20)
        ax.set_xlabel(r"$t \; \mathrm{[ms]}$", size = 20)
        ax.set_ylabel(r"$a \; \mathrm{[m]}$", size = 20)
        ax.set_ylim(0.00, 0.10)
        ax.tick_params(axis = "both", labelsize = 15)
        ax.grid(alpha=0.5)
    
        fig.tight_layout() 



def plotPlasmaBoundary(radialShift, verticalShift, timeStart, timeEnd, plotBPP):
    if (not type(timeStart) == int) or (not type(timeEnd) == int):
        print("timeStart and timeEnd have to be integers")
        return 0
    
    numberOfBoundaries = plasmaEnd - plasmaStart    

    # dividing data into intervals for plotting
    time_forPlotting = applyMovingAverage(time, numberOfBoundaries)
    time_forPlotting = [round(time_forPlotting[i]-0.5, 1) for i in range(len(time_forPlotting))] # moving average moves each time_forPlotting element 0.5 ms further than it should be
    radialShift      = applyMovingAverage(radialShift, numberOfBoundaries)      
    verticalShift    = applyMovingAverage(verticalShift, numberOfBoundaries)
    plasmaRadius     = calculatePlasmaRadius(radialShift, verticalShift)

    # selecting the intervals we want to plot
    timeStart_index  = time_forPlotting.index(timeStart)
    timeEnd_index    = time_forPlotting.index(timeEnd)
    time_forPlotting = time_forPlotting[timeStart_index:timeEnd_index]
    radialShift      =      radialShift[timeStart_index:timeEnd_index]
    verticalShift    =    verticalShift[timeStart_index:timeEnd_index]
    plasmaRadius     =     plasmaRadius[timeStart_index:timeEnd_index]

    # calculating boundary coordinates
    theta = np.linspace(0, 2*np.pi, 100) # 100 is the number of data points in one boundary
    limiter_r = limiterDiameter * np.cos(theta) + 0.4
    limiter_z = limiterDiameter * np.sin(theta) 
    chamber_r = chamberDiameter * np.cos(theta) + 0.4
    chamber_z = chamberDiameter * np.sin(theta)
    plasmaBoundary_r = [plasmaRadius[i] * np.cos(theta) + radialShift[i] + 0.4 for i in range(len(time_forPlotting))]
    plasmaBoundary_z = [plasmaRadius[i] * np.sin(theta) + verticalShift[i] for i in range(len(time_forPlotting))]        
    
    Z_min_arr    = [round(min(plasmaBoundary_z[i]), 7) for i in range(len(time_forPlotting))]
    Z_max_arr    = [round(max(plasmaBoundary_z[i]), 7) for i in range(len(time_forPlotting))]
    Z_center_arr = [round(verticalShift[i], 7) for i in range(len(time_forPlotting))]
    print("Z_min array: "    + str(Z_min_arr))
    print("Z_max array: "    + str(Z_max_arr))
    print("Z_center array: " + str(Z_center_arr))

    # plotting the boundaries
    fig, ax = plt.subplots(1,1, figsize=(7,6))    
    ax.plot(limiter_r, limiter_z, color="blue", linewidth=6)
    ax.plot(chamber_r, chamber_z, color="blue", linewidth=6)    
    
    for i in range(len(time_forPlotting)):
        if plasmaRadius[i] < 0: 
            print("for time " + str(time_forPlotting[i]) + " ms plasma appears out of chamber")
            continue
        ax.scatter(radialShift[i] + 0.4, verticalShift[i], s = 100, marker = "x", color = (1-i/len(time_forPlotting), 1-i/len(time_forPlotting), 0))
        ax.plot(plasmaBoundary_r[i], plasmaBoundary_z[i], label=f"${time_forPlotting[i]} - {time_forPlotting[i]+1}  \; \mathrm{{ms}}$", linewidth = 4, color = (1-i/len(time_forPlotting), 1-i/len(time_forPlotting), 0))
        ax.set_title(fr"\textbf{{\#{str(shotNumber)}}}", size = 20)
        ax.set_xlabel(r"$R \; \mathrm{[m]}$", size = 20)
        ax.set_ylabel(r"$Z \; \mathrm{[m]}$", size = 20)
        ax.tick_params(axis = "both", labelsize = 17)
        ax.axis("equal")
    if plotBPP == True: 
        ax.plot([0.4, 0.4], [-0.0996, BPP_Z], color="red", linewidth=7, label=fr"$Z = {(BPP_Z + 0.0022)*100}$ cm")

    ax.grid(alpha = 0.25)
    ax.legend(loc="lower right", prop={'size': 13})
    fig.tight_layout()
    # plt.savefig(f"/home/maty/Plocha/TokGol_2022_23/python/plasmaPosition_figures/PLASMA_BOUNDARY/plasma_boundary_shot_{shotNumber}.pdf")



# -----------------------------------------------------------------------------

# 35788, 35792, 35794, 35797, 35798, 35801, 35802, 35806, vacuum = 35746
# 26355,26356,26357,26358,26359,26360,26361,26362,26366,26365, vacuum = 26369

shotNumber                  = 39932 #26366
vacuumShotNumber            = 39109 #26369
distanceFromChamberCenterMC = 0.093  # m
effectiveAreaMC             = 0.0038 # m^2
limiterDiameter             = 0.085  # m
chamberDiameter             = 0.100  # m
BPP_Z                       = -0.095 - 0.0022 # m (the -0.0022 is there because the linewidth is too large and the ending of the line isn't accurate)


allVoltages, allVacuumVoltages = loadData(shotNumber, vacuumShotNumber)
print("t1")
allMagneticFields = calculateMagneticField(allVoltages, allVacuumVoltages)
print("t2")
radialShift, verticalShift = calculateCenerShifts(allMagneticFields)
print("t3")

# plotCenterShifts(radialShift, verticalShift, plotPlasmaRadus=False)

plotPlasmaBoundary(radialShift, verticalShift, 9, 14, plotBPP=False)
plt.show()