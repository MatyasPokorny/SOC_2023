# THIS IS A FILE FOR PLOTTING THE .hdf5 FILE DATA OF TOKAMAK GOLEM PARAMS

import numpy as np
import matplotlib.pyplot as plt
import h5py

file_name = "GOLEM_params.h5"
path = f"/home/maty/Plocha/TokGol_2022_23/NICE_parameters/GOLEM/{file_name}"
TokaMesh_GOLEM = h5py.File(path, "r")

#------------------------------------------------------------------------------

plt.rcParams['text.usetex'] = True
plt.rcParams['font.family'] = "Times New Roman"

c = 2 # defines plot and text size

fig, ax = plt.subplots(figsize=(10*c,10*c))

#--------------
# chamber plot:
ax.scatter(0.4, 0, marker="x", s=300, color="black")
ax.plot(list(TokaMesh_GOLEM["limiter"]["r"])               , list(TokaMesh_GOLEM["limiter"]["z"])               , linewidth=3, label=r"$\mathrm{limiter}$")
ax.plot(list(TokaMesh_GOLEM["vessel/0"]["outline_inner_r"]), list(TokaMesh_GOLEM["vessel/0"]["outline_inner_z"]), linewidth=3, label=r"$\mathrm{chamber}$", color="red")
ax.plot(list(TokaMesh_GOLEM["vessel/0"]["outline_outer_r"]), list(TokaMesh_GOLEM["vessel/0"]["outline_outer_z"]), linewidth=3, color="red")

#----------------
# iron core plot:
len1 = len(list(TokaMesh_GOLEM["iron_core/segment"]))
iron_core_r = []
iron_core_z = []
for i in range(len1):
    iron_core_r.append(list(TokaMesh_GOLEM[f"iron_core/segment/{i}/geometry/outline"]["r"]))
    iron_core_z.append(list(TokaMesh_GOLEM[f"iron_core/segment/{i}/geometry/outline"]["z"]))    
#for i in range(len1):
    ax.plot(iron_core_r[i], iron_core_z[i]                                                , linewidth=3, color="black")
    ax.plot([iron_core_r[i][3], iron_core_r[i][0]], [iron_core_z[i][3], iron_core_z[i][0]], linewidth=3, color="black")
ax.plot([iron_core_r[1][3], iron_core_r[1][0]], [iron_core_z[1][3], iron_core_z[1][0]], linewidth=3, color="black", label=r"$\mathrm{iron \; core}$")
# line above serves the only purpose of adding an element to the legend

#---------------
# PF coils plot:
len2 = len(list(TokaMesh_GOLEM["pf_active/coils"]))
pf_active_r = []
pf_active_z = []
pf_active_dr = []
pf_active_dz = []
pf_active_r_all = []
pf_active_z_all = []
for i in range(len2): # loop loads and prepares pf_coils data for plotting
    pf_active_r.append(np.array(TokaMesh_GOLEM[f"pf_active/coils/{i}/r"]))
    pf_active_z.append(np.array(TokaMesh_GOLEM[f"pf_active/coils/{i}/z"]))
    pf_active_dr.append(np.array(TokaMesh_GOLEM[f"pf_active/coils/{i}/dr"]))
    pf_active_dz.append(np.array(TokaMesh_GOLEM[f"pf_active/coils/{i}/dz"]))
    pf_active_r[i] = pf_active_r[i] - pf_active_dr[i]/2
    pf_active_z[i] = pf_active_z[i] - pf_active_dz[i]/2 
    pf_active_r_all.append([pf_active_r[i], pf_active_r[i] + pf_active_dr[i], pf_active_r[i] + pf_active_dr[i], pf_active_r[i]])
    pf_active_z_all.append([pf_active_z[i], pf_active_z[i], pf_active_z[i] + pf_active_dz[i], pf_active_z[i] + pf_active_dz[i]])
#for i in range(len2):
    ax.plot(pf_active_r_all[i], pf_active_z_all[i]                                                        , linewidth=3, color="cyan")
    ax.plot([pf_active_r_all[i][3], pf_active_r_all[i][0]], [pf_active_z_all[i][3], pf_active_z_all[i][0]], linewidth=3, color="cyan")
ax.plot([pf_active_r_all[1][3], pf_active_r_all[1][0]], [pf_active_z_all[1][3], pf_active_z_all[1][0]], linewidth=3, color="cyan", label=r"$B_\theta$ $\mathrm{coils}$")
# line above serves the only purpose of adding an element to the legend





# #--------------------------
# # MHD ring plot:
# len3 = len(list(TokaMesh_GOLEM["diag/MHD_ring"]))
# MHD_ring_r = []
# MHD_ring_z = []
# MHD_ring_dr = []
# MHD_ring_dz = []
# MHD_ring_r_all = []
# MHD_ring_z_all = []

# for i in range(len3):
#     MHD_ring_r.append(np.array(TokaMesh_GOLEM[f"diag/MHD_ring/{i}/r"]))
#     MHD_ring_z.append(np.array(TokaMesh_GOLEM[f"diag/MHD_ring/{i}/z"]))
#     MHD_ring_dr.append(np.array(TokaMesh_GOLEM[f"diag/MHD_ring/{i}/dr"]))
#     MHD_ring_dz.append(np.array(TokaMesh_GOLEM[f"diag/MHD_ring/{i}/dz"]))
    
#     MHD_ring_r_all.append( [MHD_ring_r[i], MHD_ring_r[i] + MHD_ring_dr[i], MHD_ring_r[i] + MHD_ring_dr[i], MHD_ring_r[i], MHD_ring_r[i]] )
#     MHD_ring_z_all.append( [MHD_ring_z[i], MHD_ring_z[i], MHD_ring_z[i] + MHD_ring_dz[i], MHD_ring_z[i] + MHD_ring_dz[i], MHD_ring_z[i]] )    

#     ax.plot(MHD_ring_r_all[i], MHD_ring_z_all[i], color="orange")
# ax.plot(MHD_ring_r_all[1], MHD_ring_z_all[1], color="orange", label=r"$\mathrm{MHD \; ring}$")


#--------------------------
# Mirnov coils plot:
    
len4 = len(list(TokaMesh_GOLEM["diag/Mirnov"]))
Mirnov_r = []
Mirnov_z = []
Mirnov_dr = []
Mirnov_dz = []
Mirnov_r_all = []
Mirnov_z_all = []

for i in range(len4):
    Mirnov_r.append(np.array(TokaMesh_GOLEM[f"diag/Mirnov/{i}/r"]))
    Mirnov_z.append(np.array(TokaMesh_GOLEM[f"diag/Mirnov/{i}/z"]))
    Mirnov_dr.append(np.array(TokaMesh_GOLEM[f"diag/Mirnov/{i}/dr"]))
    Mirnov_dz.append(np.array(TokaMesh_GOLEM[f"diag/Mirnov/{i}/dz"]))
    
    Mirnov_r_all.append( [Mirnov_r[i], Mirnov_r[i] + Mirnov_dr[i], Mirnov_r[i] + Mirnov_dr[i], Mirnov_r[i], Mirnov_r[i]] )
    Mirnov_z_all.append( [Mirnov_z[i], Mirnov_z[i], Mirnov_z[i] + Mirnov_dz[i], Mirnov_z[i] + Mirnov_dz[i], Mirnov_z[i]] )    

    ax.plot(Mirnov_r_all[i], Mirnov_z_all[i], color="orange")
ax.plot(Mirnov_r_all[1], Mirnov_z_all[1], color="orange", label=r"$\mathrm{Mirnov \; coils}$")






#---------------------------------------------------

ax.set_title(r"\textbf{GOLEM scheme (without stab)}", fontsize=30*c)
ax.set_xlabel("$R \; [\mathrm{m}]$", fontsize=30*c, labelpad=5*c)
ax.set_ylabel("$Z \; [\mathrm{m}]$", fontsize=30*c, labelpad=5*c)
ax.tick_params(axis="both", labelsize=25*c)
ax.axis("equal")
ax.grid(alpha=0.35) #alpha=0.75
ax.legend(fontsize=25*c, frameon = False, loc="center right") # legend labels in math mode only because it looks nice
ax.set_xlim(0, 1)
plt.tight_layout

# # for chamber plot only:
# ax.set_xlim(0.4-0.05, 0.4+0.05)
# ax.set_ylim(-0.15, 0.15)
# ax.legend([])
# # ---

#plt.savefig("/home/maty/Plocha/TokGol_2022_23/figures/TokaMesh_GOLEM.pdf")
plt.show()



TokaMesh_GOLEM.close()
