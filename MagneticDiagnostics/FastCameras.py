# FILE FOR PLOTTING IMAGE FROM FAST CAMERAS (AND USING IT TO DETERMINE PLASMA POSITION)
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import PIL
import urllib.request
plt.rcParams["text.usetex"] = True
plt.rcParams["font.family"] = "Times New Roman"
"""
When using this file, the user has to do a lot of manual work. After changing the shot number, user has to:
1. change plasmaStart_location and plasmaEnd_location variables according to the presented table                            ... section CREATING TIME AXIS
2. change min, max and center labels by uncommenting 3 lines corresponding with the chosen shot (and commenting all others) ... section PLASMA BOUNDARY VIA MIRNOV COILS

! In its present form, this file works only for the shot session analysed during the 2022/23 SOČ work. At the bottom of the script, data loading and plotting for recent (29.3.23) shots.
"""


#--------------------
# SOČ SESSION: 26355, 26356, 26357, 26358, 26359, 26360, 26361, 26362, 26366, 26365

shotNumber = 26361
chamberRadius = 9.75 # cm


#--------------
# LOADING DATA:

# fastCamerasImage_vertical = plt.imread(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/diagnostics/Radiation/0211FastCamera.ON/1/CorrectedRGB.png")
fastCamerasImage_vertical = np.array(PIL.Image.open(urllib.request.urlopen(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/diagnostics/Radiation/0211FastCamera.ON/1/CorrectedRGB.png")))

loop_voltage              = pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{shotNumber}/loop_voltage", sep="\t", names=["time", "U_loop"])
plasmaStart_label = pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{shotNumber}/plasma_start")
plasmaStart_label = float(plasmaStart_label.columns[0]) * 1000
plasmaEnd_label   = pd.read_csv(f"http://golem.fjfi.cvut.cz/utils/data/{shotNumber}/plasma_end")
plasmaEnd_label   = float(plasmaEnd_label.columns[0]) * 1000
print("plasma start: " + str(plasmaStart_label) + " ms; plasma end: " + str(plasmaEnd_label) + " ms")


#--------------------
# CREATING TIME AXIS:

plasmaStart_location = 35
plasmaEnd_location = 1280

"""
LOC        START    END
#26355.....5........1300
#26361.....35.......1280
#26362.....40.......1410
#26365.....75.......1470
#26366.....85.......1415
"""

def label_to_location_time(label):
    a = (plasmaStart_location - plasmaEnd_location) / (plasmaStart_label - plasmaEnd_label)    
    b = plasmaStart_location - a * plasmaStart_label
    location = a * label + b
    return location

time_label    = np.arange(0.0 , 20.0 , 0.01)
time_location = np.linspace(label_to_location_time(0.0), label_to_location_time(20.0), len(time_label))
time_label    = list(time_label)[::100]
time_location = list(time_location)[::100]


#----------------------------------
# PLASMA BOUNDARY VIA MIRNOV COILS:

def label_to_location_vertical(label):
    a = -(fastCamerasImage_vertical.shape[0]-1) / (chamberRadius*2)
    b = -chamberRadius * a
    return a * label + b

MirnovCoils_time_label = [9.5, 10.5, 11.5, 12.5, 13.5]
MirnovCoils_time_location = [label_to_location_time(MirnovCoils_time_label[i]) for i in range(len(MirnovCoils_time_label))]

# SHOT 26355
# MirnovCoils_vertical_label_max = np.array([0.0366624,   0.04119,  0.0433112,  0.044360,  0.0447508])*100
# MirnovCoils_vertical_label_min = np.array([-0.0713762, -0.0679923, -0.0661557, -0.0644983, -0.0626177])*100
# MirnovCoils_vertical_label_center = np.array([-0.017357, -0.013401, -0.011422, -0.010069, -0.008933])*100

# SHOT 26361
MirnovCoils_vertical_label_max = np.array([0.0222299,   0.03634958,  0.04112831,  0.043727910, 0.04475560])*100
MirnovCoils_vertical_label_min = np.array([-0.08145328, -0.07593342, -0.0722497, -0.0693825, -0.0666083])*100
MirnovCoils_vertical_label_center = np.array([-0.029612, -0.019792, -0.015561, -0.012827, -0.010926])*100

# SHOT 26362
# MirnovCoils_vertical_label_max = np.array([0.04689,    0.05102,  0.05237,  0.05237,  0.05322])*100
# MirnovCoils_vertical_label_min = np.array([-0.0787136, -0.0733638, -0.0698494, -0.0670999, -0.0645610])*100
# MirnovCoils_vertical_label_center = np.array([-0.015908, -0.01117, -0.008738, -0.006948, -0.005669])*100

# SHOT 26365
# MirnovCoils_vertical_label_max = np.array([-0.0849042, -0.0790785, -0.0735283, -0.0701591, -0.0667308])*100
# MirnovCoils_vertical_label_min = np.array([0.0648269, 0.066192, 0.0644388, 0.0635862, 0.0628795])*100
# MirnovCoils_vertical_label_center = np.array([-0.0100387, -0.0064432, -0.0045447, -0.0032864, -0.0019257])*100

# SHOT 26366
# MirnovCoils_vertical_label_max = np.array([0.0732085,   0.0762190,  0.072833,  0.071027, 0.0695939])*100
# MirnovCoils_vertical_label_min = np.array([-0.08247386, -0.08172335, -0.0759413, -0.0717998, -0.06828004])*100
# MirnovCoils_vertical_label_center = np.array([-0.004633, -0.002752, -0.001554, -0.000386, 0.000657])*100

MirnovCoils_vertical_location_max =    [label_to_location_vertical(MirnovCoils_vertical_label_max[i] - MirnovCoils_vertical_label_center[0]) for i in range(len(MirnovCoils_vertical_label_max))]
MirnovCoils_vertical_location_min =    [label_to_location_vertical(MirnovCoils_vertical_label_min[i] - MirnovCoils_vertical_label_center[0]) for i in range(len(MirnovCoils_vertical_label_min))]
MirnovCoils_vertical_location_center = [label_to_location_vertical(MirnovCoils_vertical_label_center[i] - MirnovCoils_vertical_label_center[0]) for i in range(len(MirnovCoils_vertical_label_center))]


#------------------------------------
# PLOTTING THE OLD FAST CAMERAS IMAGE:

fig, ax = plt.subplots(1,1, figsize=(18*0.8, 5*0.8))

ax.plot(MirnovCoils_time_location, MirnovCoils_vertical_location_max, color="white")
ax.plot(MirnovCoils_time_location, MirnovCoils_vertical_location_min, color="white")
# ax.plot(MirnovCoils_time_location, MirnovCoils_vertical_location_center, color="white")
# ax.plot(MirnovCoils_time_location, [label_to_location_vertical(0) for i in range(len(MirnovCoils_time_location))], color="white")

ax.set_xticks(time_location)
ax.set_xticklabels(time_label)
ax.set_yticks([0, int(fastCamerasImage_vertical.shape[0]/2), fastCamerasImage_vertical.shape[0]-1])
ax.set_yticklabels([chamberRadius, 0.0, -chamberRadius])
ax.tick_params(axis="both", labelsize=20)

ax.set_title(fr"\textbf{{Vertikální poloha (\#{shotNumber})}}", size=25)
ax.set_xlabel(r"$t \; \mathrm{[ms]}$", size=25)
ax.set_ylabel(r"$Z \; \mathrm{[cm]}$", size=25)
ax.imshow(fastCamerasImage_vertical)

fig.tight_layout()
#fig.savefig(f"/home/maty/Plocha/TokGol_2022_23/python/plasmaPosition_figures/FAST_CAMERAS/fast_cameras_shot_{shotNumber}.pdf")
plt.show()


















#------------------------------------------------------------------------------
# =============================================================================

# LOADING DATA FOR THE CURRENT SESSIONS

# fastCamerasImage_radial = plt.imread(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/Diagnostics/FastCameras/plasma-film_Radial2.png")
# fastCamerasImage_vertical = plt.imread(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/Diagnostics/FastCameras/plasma-film_Vertical2.png")
# loopVolage = pd.read_csv(f"http://golem.fjfi.cvut.cz/shots/{shotNumber}/Diagnostics/BasicDiagnostics/U_Loop.csv",names=["time", "U_loop"])

# plasmaStart_label = pd.read_csv("http://golem.fjfi.cvut.cz/shots/39146/Diagnostics/BasicDiagnostics/Results/t_plasma_start")
# plasmaStart_label = float(plasmaStart_label.columns[0])
# plasmaEnd_label = pd.read_csv("http://golem.fjfi.cvut.cz/shots/39146/Diagnostics/BasicDiagnostics/Results/t_plasma_end")
# plasmaEnd_label = float(plasmaEnd_label.columns[0])

# =============================================================================

# =============================================================================

# PLOTTING THE CURRENT FAST CAMERAS IMAGES

# fig, ax = plt.subplots(2, 1, figsize=(20*0.9, 9*0.9))
# 
# ax[0].set_xticks(time_location)
# ax[0].set_xticklabels(time_label)
# ax[0].set_yticks([0, int(fastCamerasImage_radial.shape[0]/2), fastCamerasImage_radial.shape[0]-1])
# ax[0].set_yticklabels([9.5, 0.0, -9.5])
# ax[0].tick_params(axis="both", labelsize=20)
# 
# ax[0].set_title(fr"\textbf{{Radial position (shot {shotNumber})}}", size=25)
# ax[0].set_xlabel(r"$t \; \mathrm{[ms]}$", size=25)
# ax[0].set_ylabel(r"$r \; \mathrm{[cm]}$", size=25)
# ax[0].imshow(fastCamerasImage_radial)
# 
# 
# ax[1].set_xticks(time_location)
# ax[1].set_xticklabels(time_label)
# ax[1].set_yticks([0, int(fastCamerasImage_vertical.shape[0]/2), fastCamerasImage_vertical.shape[0]-1])
# ax[1].set_yticklabels([-9.5, 0.0, 9.5])
# ax[1].tick_params(axis="both", labelsize=20)
# 
# ax[1].set_title(fr"\textbf{{Vertical position (shot {shotNumber})}}", size=25)
# ax[1].set_xlabel(r"$t \; \mathrm{[ms]}$", size=25)
# ax[1].set_ylabel(r"$Z \; \mathrm{[cm]}$", size=25)
# ax[1].imshow(fastCamerasImage_vertical)
# 
# fig.tight_layout()
# fig.savefig(f"/home/maty/Plocha/TokGol_2022_23/python/plasmaPosition_figures/FAST_CAMERAS/fast_cameras_shot_{shotNumber}.pdf")

# =============================================================================
