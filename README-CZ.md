# Měření a simulace polohy plazmatu na tokamaku GOLEM

Na tomto repozitáři se nachází má SOČ práce z roku 2022/23 zpracovaná na [tokamaku GOLEM](http://golem.fjfi.cvut.cz/wiki/). V rámci této práce byla především zprovozněna část [programu NICE](https://hal.science/hal-02734719v1/document) (*Newton direct and Inverse Computation for Equilibrium*), který umožňuje simulovat magnetické pole tokamaku (resp. jeho geometrii). Byly zprovozěny jeho režimy *inverse* a *direct* (static, nikoliv kvazi-static), které umí vypočítat magnetické pole tokamaku na základě zadaného okraje plazmatu / na základě zadaných proudů v poloidálních cívkách. Práce je stručně shrnuta [zde](https://youtu.be/--OCZOzaCg4). Soubor `SOC2023_MatyasPokorny.pdf` obsahuje samotnou SOČ práci. Ve složkách `/MagneticDiagnostics` a `/NICE` je nahraná většina Python skriptů, které jsem v rámci práce sepsal.

---

### Experimentální měření polohy plazmatu

Ve složce `/MagneticDiagnostics` se nachází soubory:
- `BasicParameters.py` tvoří grafy základních makroskopických parametrů
- `BallPenProbe.py`, `FastCameras.py`, `MirnovCoils_v1.py` zpracovávají data (a následně tvoří grafy) ze tří diagnostik polohy plazmatu
- `MirnovCoils_v2.py` tvoří kvalitnější grafy než `MirnovCoils_v1.py` ale je náročněji využitelný
- složka `/example_figs` obsahuje několik grafů, které ilustrují funkce všech nahraných skriptů

---

### Simulace polohy plazmatu

Ve složce `/NICE` se nachází:
- `/input` a `/output` obsahují ilustrační vstupní a výstupní data NICE simulace tokamaku GOLEM
- `/GOLEM_model_for_MATLAB_interface` obsahuje skripty pro tvorbu a vizualizaci virtuálního modelu GOLEMa ve formě `.h5` souboru, který slouží jako vstup pro MATLAB NICE rozhraní
- `/IronCore_BZ` obsahuje soubory, které jsme využili pro porovnání vertikální složky magnetického pole *B_Z* v železném jádře GOLEMa, vypočítané pomocí NICE simulace a pomocí experimentálního měření (s fluxloopy)
- `/PythonGUI` obsahuje GUI vytvořené pomocí Pythonu a knihovny *streamlit* pro práci se zprovozněnými režimy NICE
- `/output-visualization` obsahuje Python soubory pro vizualizaci vypočtených (pomocí aproximací) a simulovaných (pomocí NICE) 1D a 2D profilů (hlavně psi, q, j_tor)
- `/example_figs` obsahuje ilustrační grafy využité v různých fázích zprovozňování NICE
- soubor `primaryTransformerCoils.py` slouží k zpracování dat z měření (4.5.23) celkového proudu v primárích transformátorových cívkách GOLEMa



#### Využití

Pro spuštění Python grafického rozhraní je nutné, aby měl uživatel staženou knihovnu *streamlit* (mimo standardních kihoven jako *numpy*, *matplotlib* viz samotné GUI):

```
pip install streamlit
```

Dále je po klonování repozitáře nutné, aby se uživatel nacházel ve složce `/NICE/PythonGUI` a spustil příkaz:

```
streamlit run main.py
```

Vygenerovaným GUI je možné procházet, není ale možné provádět samotné NICE simulace (z důvodu autorských práv). Složky `/NICE/input` a `/NICE/output` obsahují ilustrační data.
